/*
  Wap to print the following pattern:
		
 	$
       	@  @
	&  &  &
	#  #  #  #
	$  $  $  $  $
	@  @  @  @  @  @
	&  &  &  &  &  &  &
	#  #  #  #  #  #  #  #	
  
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter The Number of Rows::");
		int rows= Integer.parseInt(br.readLine());
			
		int count=1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if(count==4){
					System.out.print("#"+"\t");
				}else if(count==3){
					System.out.print("&"+"\t");
				}else if(count==2){
					System.out.print("@"+"\t");
				}else{
					System.out.print("$"+"\t");
				}
			
			}
			if(count==4){
				count=0;
			}
			count++;																													

			 
			System.out.println();
		}
	}
}
