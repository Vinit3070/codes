/*
 Wap to print the Following Pattern-
	
	O	
	14	13
	L	K	J	
	9	8	7	6	
	E	D	C	B	A
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Rows::");
		int rows= Integer.parseInt(br.readLine());
		
		int num=(rows*rows+rows)/2;
		char ch=(char)(64+num); 

		for(int i=1;i<=rows;i++){
			if(rows%2!=0){
				for(int j=1;j<=i;j++){
					if(i%2!=0){
						System.out.print("\t"+ch);
					}else{
						System.out.print("\t"+num);
					}
					num--;
					ch--;
				}
			}
			else{
				for(int j=1;j<=i;j++){
					if(i%2==0){
						System.out.print("\t"+ch);
					}else{
						System.out.print("\t"+num);
					}
					num--;
					ch--;
				}
			}
	
			System.out.println();
		}
	}
}
