//print the prime numbers between given range
//start =10     &     end=100
//prime no's= 11,13,17,19....

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.print("Enter the Start & End::");

		int start= Integer.parseInt(br.readLine());
		int end= Integer.parseInt(br.readLine());

		for(int i=start; i<=end ;i++){
			int count=0;
			for(int j=1;j<=i;j++){
				if(i%j==0){
					count++;
				}
			}
			if(count==2){
			System.out.print(i+" ");
			}
		}

	}
}
