/*
 Wap to take a number as an input and print the addition of factorials of each digit from that number
	IP-1234
	OP- The Addition of Factorials of each digit of Number 1234 is =33
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Number::");
		int no=Integer.parseInt(br.readLine());

		int sum=0;
		
		/*while (no!=0){
			int fact=1;
			int N=no%10;

			for(int i=1;i<=N;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			no=no/10;
		}*/

		for(int i=no;i!=0;i=i/10){

			int rem =i%10;
			int fact=1;
			for(int j=1;j<=rem;j++){
				fact=fact*j;
			}
			sum=sum+fact;
		}
		System.out.println("The Addition of Factorials of each digit of "+no+" is: "+sum);
	}
}
