/*
	Wap to print all even numbers in reverse order between given range 
	and Print all odd numbers in Standard Way
	
	start=2
	end=9

	Even= 8  6  4  2
	Odd = 3  5  7  9
*/

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Start::");
		int start=Integer.parseInt(br.readLine());
		
		System.out.print("Enter End::");
		int end=Integer.parseInt(br.readLine());
		
		System.out.print("Even:: ");
		for(int i=end;i>=start;i--){

			if(i%2==0){
				System.out.print(i+" ");
			}
		}
		System.out.println();
		System.out.print("ODD:: ");
		
		
		for(int i=start;i<=end;i++){
			if(i%2!=0){
				System.out.print(i+" ");
			}
		}	

		System.out.println();

	}

}
