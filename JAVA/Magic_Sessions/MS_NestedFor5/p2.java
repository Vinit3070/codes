/*
 Q.2 Print the Following Pattern-
 	
 	#  =  =  =  =
	=  #  =  =  =
	=  =  #  =  =
	=  =  =  #  =
	=  =  =  =  #
*/

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Number of Rows::");
		int rows= Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){

			for(int j=1;j<=rows;j++){

				if(i==j){
					System.out.print("\t"+"#");
				}else{
					System.out.print("\t"+"=");
				}
			}
			System.out.println();
		}
	}
}
	
	
