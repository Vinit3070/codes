/*
	Wap ,that takes two characters from users and if the characters are equal the print as it is
	otherwise print its Differnce.


*/
import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter Char1 and Char2::");
		char ch1=(char)br.read();
		br.skip(1);
		char ch2=(char)br.read();

		if(ch1==ch2){
			System.out.print("The characters"+ch1+" & "+ch2+" are Equal");
		}else{
			int diff=ch2-ch1;
			System.out.print("The Differnce Between  "+ch1+" & "+ch2+ "  is: "+diff);
		}
		System.out.println();

	}
}
