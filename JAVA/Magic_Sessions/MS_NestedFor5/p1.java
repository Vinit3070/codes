	
/*
 Q.1- Print the Following Pattern-

	D4	C3 	B2	A1
	A1	B2	C3	D4
	D4	C3	B2	A1
	A1	B2	C3	D4
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Number of Rows::");
		int rows=Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
			
			int num=rows;
			char ch=(char)(64+rows);

			for(int j=1;j<=rows;j++){
				if(i%2==1){
					System.out.print("\t"+ch-- + num--);
				}else{
					System.out.print("\t"+(char)(64+j)+j);
				}
				
			}
			System.out.println();
		}
	}
}
