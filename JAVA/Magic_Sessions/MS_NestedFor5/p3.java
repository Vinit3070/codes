/*
 Q.3 Print the Following Pattern
	
	5	4	3	2	1
	8	6	4	2
	9	6	3
	8	4
	5
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter The Number of Rows::");
		int rows= Integer.parseInt(br.readLine());
		
		int n1=rows;

		for(int i=1;i<=rows;i++){

			int temp=n1*i;

			for(int j=1;j<=rows-i+1;j++){
				
				System.out.print(temp+"\t");
				temp=temp-i;	
			}

			n1--;
			System.out.println();

		}
	}
}
