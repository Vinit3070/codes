/*

	Wap to print the Composite Numbers between the given range
*/

import java.util.*;
class Solution{

	public static void main(String[] args){
		Scanner sc=new Scanner (System.in);

		System.out.print("Enter from where you want to Start ::");
		int start=sc.nextInt();
		System.out.print("Enter where you want to End ::");
		int end=sc.nextInt();
		
		int count=0;
		int sum=0;
		for(int i=start;i<=end;i++){
			for(int j=1;j<=i;j++){
				if(i%j==0)
					count++;
			}

			if(count>2){
				sum=sum+i;
			}
			count=0;
			
		}
		System.out.println("Sum of composite numbers between the range is:: "+sum);
	}

}
