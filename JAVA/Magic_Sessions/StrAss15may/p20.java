/*
	Wap to take input from the user, and check whether the given string is palindrome or not

*/
import java.util.*;
class Solution{

	static boolean StringPalindrome(String str){

	
		String rev ="";
		char[] ch = str.toCharArray();
	
		for(int i=0;i<str.length();i++){
			
			rev = str.charAt(i) + rev;
		}

		if(rev.equals(str)) 
			return true;
		else
			return false;

	}

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the String::");
		String str=sc.nextLine();

		//StringPalindrome(str);	
		//System.out.println("");	
		
		if(StringPalindrome(str)) 
			System.out.println("PALINDROME");
		else
			System.out.println("NOT PALINDROME");
	}
}
