/*
	Wap to take two strings from user and compare the strings are equal or not 
*/

import java.io.*;
class Solution{
	
	static void myStrCmp(String str1,String str2){
		
		char ch1[]=str1.toCharArray();
		char ch2[]=str2.toCharArray();

		int flag =0;
		for(int i=0;i<ch1.length;i++){
			if(ch1.length==ch2.length){
				if(ch1[i]==ch2[i])
					flag=1;
			}	
			else{
				flag =0;	
			}
		}

		if(flag==1){
			System.out.println("The Strings are Equal..");
		}
		else{
			System.out.println("The Strings are Not equal..");
		}
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Two strings::");
		String str1=new String(br.readLine());
		String str2=new String(br.readLine());

		myStrCmp(str1,str2);
	}
}
