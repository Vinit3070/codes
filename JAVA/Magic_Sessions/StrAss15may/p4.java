/*
 	Wap to print the square roots of numbers in given range 
*/

import java.util.*;
class Solution{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.print("Enter the Starting range::");
		int start=sc.nextInt();
		System.out.print("Enter the Ending range::");
		int end=sc.nextInt();

		System.out.println("Square roots of Numbers is Given Range::");

		for(int i=start;i<=end;i++){
				
			System.out.println(Math.sqrt(i)+"  ");
		}
	}
}
