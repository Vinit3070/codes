/*
	Wap to take Input String and count the vowels and Consonants from the given string.
*/
import java.io.*;
class Solution{

	static void CntStrVowCon(String str){
		char ch[]=str.toCharArray();

		int vCount=0;
		int cCount=0;
		for(int i=0;i<ch.length;i++){
			if(ch[i]=='a'||ch[i]=='A'||ch[i]=='e'||ch[i]=='E'||ch[i]=='i'||ch[i]=='I'||ch[i]=='o'||ch[i]=='O'||ch[i]=='u'||ch[i]=='U'){
				vCount++;
			}
			else{
				cCount++;
			}

		}

		System.out.println("Vowels Count in given string- "+vCount);
		System.out.println("Consonants Count in given string- "+cCount);
	}
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the String::");
		String str=new String(br.readLine());

		CntStrVowCon(str);
	}
}
