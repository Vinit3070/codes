/*WAP to print the following pattern-
	
  	*
  	* *
  	* * *
  	* *
  	*
 
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.print("ENter the no of Rows::");
		int rows=Integer.parseInt(br.readLine());
		
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print("*  ");
			}
			System.out.println();
		}

		for(int i=rows-1;i>=0;i--){
			for(int j=1;j<=i;j++){
				System.out.print("*  ");
			
			}
			System.out.println();
		}
	}
}
