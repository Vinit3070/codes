/*
	Wap to print odd elements cube in an array
*/

import java.io.*;
class Solution{

	static void arrEleCub(int arr[]){
		System.out.print("Odd Elements Cube in an Array are:: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2!=0){
				System.out.print(" "+arr[i]*arr[i]*arr[i]);
			}
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of an array::");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		arrEleCub(arr);

		System.out.println();

	}
}
