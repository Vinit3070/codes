/*
	Wap to take input from the user, and check whether the given string is palindrome or not

*/
import java.util.*;
class Solution{

	static boolean StringPalindrome(String str){

		//char arr[]=str.toCharArray();
		//char arr1[]=str.toCharArray();
		String rev ="";
		
	
		for(int i=0;i<str.length();i++){
			rev=rev+str.charAt(i);
		}

		if (str.equals(rev)){
			System.out.println("The String is Palindrome String..");
			return true;
		}
		else{
			System.out.println("The String is not a Palindrome String..");
			return false;
		}
	}

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the String::");
		String str=sc.nextLine();

		StringPalindrome(str);	
		//System.out.println("");	
	}
}
