/*	
	Wap to take input fromnuser in 3d array and skip the diagonal elements while printing the array
		
		1  2  3
		4  5  6 
		7  8  9

	OP- 2,3,4,6,7,8
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader (new InputStreamReader(System.in));
		
		System.out.println("Enter the Plane in an Array::");
		int plane= Integer.parseInt(br.readLine());

		System.out.println("Enter the Rows in an Array::");
		int row= Integer.parseInt(br.readLine());

		System.out.println("Enter the Columns in an Array::");
		int col= Integer.parseInt(br.readLine());
		
		int arr[][][]=new int[plane][row][col];
		
		System.out.println("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				for(int k=0;k<arr[i][j].length;k++){
					
					arr[i][j][k]=Integer.parseInt(br.readLine());
				
				}
			}
		}

		System.out.println("Array ELements are::");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				for(int k=0;k<arr[i][j].length;k++){
					
						System.out.print(arr[i][j][k]+" ");
				
				}
				System.out.println();
			}
		}

		System.out.println();


		System.out.print("Array ELements Excluding Diagonal Elements are::");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				for(int k=0;k<arr[i][j].length;k++){
					if(j!=k)
						System.out.print(arr[i][j][k]+" ");
				
				}
			}
		}

		System.out.println();



	}
}
