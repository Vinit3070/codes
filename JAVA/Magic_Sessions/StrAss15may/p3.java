/*
 
   Wap to print the Pattern-
   	
   	a
	A  b
	a  b  c
	A  B  C  D
*/
import java.util.*;
class Solution{
	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no of rows:");
		int rows=sc.nextInt();

		for(int i=1;i<=rows;i++){
			char ch='a';
			char ch1='A';
			for(int j=1;j<=i;j++){
				if(i%2!=0){
					System.out.print("\t"+ch);
					ch++;
					
				}else{
					System.out.print("\t"+ch1);
					ch1++;
				}
				
			}
			System.out.println();
		}

	}
}
