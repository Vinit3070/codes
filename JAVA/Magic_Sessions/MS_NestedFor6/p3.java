/*
 
   WAP to take a range from user and print the perfect squares between that given range

*/

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Starting Range::");
		int start= Integer.parseInt(br.readLine());

		System.out.print("Enter the Ending Range::");
		int end=Integer.parseInt(br.readLine());
		
		System.out.println("Perfect Squares Between "+start+ " & "+end+" are:: ");
		for(int i=start;i<=end;i++){
			for(int j=1; j*j<=i ;j++){
				if(j*j==i){
					System.out.print(i+" ");
				}
			}
		}
		System.out.println();
	}
}
