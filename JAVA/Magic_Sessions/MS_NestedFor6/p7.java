/*
 
   Wap to take range as a input from user and print the reverse of all numbers
   
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Start Range::");
		int start= Integer.parseInt(br.readLine());
		System.out.print("Enter the end Range::");
		int end= Integer.parseInt(br.readLine());

		for(int i=start;i<=end ;i++){
			int num=i;
			int rev=0;
			
			
				while(num!=0){
					int rem= num%10;
					rev = rev*10+rem;
					num=num/10;

				

					if(rem==0){
						System.out.print(0+rev);
					}
				}

					System.out.print(rev+" ");
				}
		
		System.out.println();
	}
	
}

