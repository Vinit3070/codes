/*
	
  WAP to take a range from user and print the perfect numbers 
   Perfect Number = (1+2+3 =6)
  	so ,6 is perfect no. 
*/
import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting range::");
		int start= Integer.parseInt(br.readLine());

		System.out.println("Enter the Ending range::");
		int end= Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){
			int sum=0;
			for(int j=1;j<i;j++){
				if(i%j==0){
					sum=j+sum;
				}

			}

			if (sum==i){
				System.out.print(i+" ");
			}
	
		}

	}
}
