
/*

	WAP to take a range from user and print the composite numbers in between this range

*/	

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader (new InputStreamReader(System.in));
		
		System.out.print("Enter the Starting Range::");
		int start= Integer.parseInt(br.readLine());

		System.out.print("Enter the Ending Range::");
		int end= Integer.parseInt(br.readLine());	
		//int count=0;
		
		for (int i=start;i<=end;i++){
			int count=0;
			for(int j=1; j*j<=i ;j++){
				if(i%j==0){
					count++;
			
			}
			if(count>=2){
				System.out.print(i+" ");
			}
			
		}
			
		}
		System.out.println();
	}
}
