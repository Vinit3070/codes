/*
 
  Wap to take a range from user and print armstrong numbers between them.
  153=(1*1*1 + 5*5*5 + 3*3*3)
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Starting Range::");
		int start=Integer.parseInt(br.readLine());
		System.out.print("Enter Ending Range::");
		int end=Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){
			int temp1=i;
			int temp2=i;
			int count=0;
			int sum=0;

			while(temp1!=0){
				count++;
				temp1=temp1/10;
			}

			int mul=0;

			for(int j=temp2;j!=0;j=j/10){
				mul=1;

				int rem=j%10;

				for(int k=1;k<=count;k++){
					
					mul=mul*rem;
				}
			
				sum=sum+mul;
			}

			if(sum==temp2){
				System.out.print(i+" ");
			}
		}
	
	}
}
