/*
 	Wap to print the series of strong numbers in Entered Range

	145 = fact of each numbers addition

*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter starting range::");
		int start=Integer.parseInt(br.readLine());
		System.out.print("Enter ending range::");
		int end=Integer.parseInt(br.readLine());
		
		//int sum=0;
		for(int i=start;i<=end;i++){
			int num=i;
			int sum=0;
			//int fact=1;
			while(num!=0){
				int fact=1;
				int rem=num%10;
				
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
			
				sum=sum+fact;
				num=num/10;
			}
		

			if(sum==i){
				System.out.print(i+" ");
			}
		}
		System.out.println();
	}
}
