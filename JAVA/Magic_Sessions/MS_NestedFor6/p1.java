/*
	WAP to print the numbers are divisible by 5 from 1 to 50 and the number is even also print the count of even numbers 
	
*/

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader (new InputStreamReader(System.in));
		
		System.out.print("Enter the Starting Range::");
		int start= Integer.parseInt(br.readLine());

		System.out.print("Enter the Ending Range::");
		int end= Integer.parseInt(br.readLine());	
		int count=0;
		for (int i=start; i<=end; i++){
			if(i%5==0 && i%2==0){
				

					System.out.print(i+" ");
					count++;
			
			}

		}
		System.out.println();
		System.out.println("COUNT::"+count);
	}
}
