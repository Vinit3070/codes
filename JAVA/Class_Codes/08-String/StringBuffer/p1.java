//String Buffer 

//If we wanna do change on same string then we use StringBuffer 
//Using this no new object is made 
//it updates original object and 
//overrides on original object

class Solution{
	public static void main(String[] args){
		StringBuffer str=new StringBuffer("Vinit");

		System.out.println(System.identityHashCode(str));	//identity hashcode of heap section

		str.append("Nikam");

		System.out.println(str);

		System.out.println(System.identityHashCode(str));	//same as before updation(append)
	}
}
