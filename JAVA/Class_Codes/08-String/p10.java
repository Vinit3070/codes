//String Concatenation in Both ways
//
class Solution{
	public static void main(String[] args){
		String str1="Vinit";
		String str2="Nikam";

		String str3=str1+str2;			//It calls append() method of StringBuilder Class
		String str4=str1.concat(str2);		//It calls concat() method of String Class

		System.out.println(str3);		//VinitNikam
		System.out.println(str4);		//VinitNikam
	}
}
