//String Concat with String Method
//
class Solution{
	public static void main(String[] args){
		String str1="Vinit";
		String str2="Nikam";

		//System.out.println(str1+str2);			//VinitNikam
		
		System.out.println(str1);		//Vinit
		System.out.println(str2);		//Nikam

	
	
		str1.concat(str2);			//It concats Strings But not store in any other string
							//so it return original ip strings
	
	
		System.out.println(str1);		//Vinit	
		System.out.println(str2);     		//Nikam
	}
}
