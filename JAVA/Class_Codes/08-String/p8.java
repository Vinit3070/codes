//String Concat with +operator

class Solution{
	public static void main(String[] args){
		String str1="Core2Web";
		String str2="Technologies";

		System.out.println(str1+str2);					//Core2WebTechnologies

		String str3="Core2WebTechnolgies";

		String str4=str1+str2;						//It concats s1+s2 and store it into another String
		System.out.println(str4);					//Core2WebTechnologies

		System.out.println(System.identityHashCode(str3));		//Heap 
		System.out.println(System.identityHashCode(str4));		//Heap
	}
}
