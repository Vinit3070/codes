
//String in Java

class Solution{
	public static void main(String[] args){
		String str1="Core2Web";			//It goes on STRING CONSTANT POOL(SCP)
							//SCP is a container which storws only unique strings
							//But if strings are same then ,it can assign it to previous one
							//It does not make new object for that 

		String str2=new String("Core2Web");	//It can Directly goes on HEAP Section
							//All things which are made by new (GOES ON HEAP)

		char str3[]={'C','2','W'};		//It goes as Integer on Integer Cache and
							//Addresses of each char is stored at HEAP Section

		System.out.println(str1);			//Core2Web
		System.out.println("STR1: "+System.identityHashCode(str1));
						
		System.out.println(str2);			//Core2Web
		System.out.println("STR2: "+System.identityHashCode(str2));
		
		System.out.println(str3);			//C2W
		System.out.println("STR3: "+System.identityHashCode(str3));
		
	}
}
