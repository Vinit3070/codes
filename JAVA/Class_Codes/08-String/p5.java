//Strings Internals

class Solution{
	public static void main(String[] args){
		char ch='A';
		int a=65;

		System.out.println(System.identityHashCode(ch));	//Internally 65 
		System.out.println(System.identityHashCode(a));		//65

		/*
		 *
		 * both goes Internally as 65 
		 * But if we want Identity hashCodes the it will call the valueOf()Method*
		 * valueOf(new character())
		 * So new object is made at this time
		 * so Identity Hashcode is Different
		 *
		 * */
	}	
}
