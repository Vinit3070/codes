//compareTo()-
//
//it compares two strings
//returns 0 if both strings are equal
//otherwise gives difference
//
class Solution{
	public static void main(String[] args){
		String str1="Vinit";
		String str2="VInit";

		System.out.println(str1.compareTo(str2));	//It returns 32 Diff (i-I)
	}
}
