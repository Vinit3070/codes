//lastIndexOf()
//
//[(ch to find),(upto index)]

import java.util.*;
class Solution{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter String::");
		String str=sc.next();				//Vinit

		System.out.println(str.lastIndexOf('i',0));	//-1
		System.out.println(str.lastIndexOf('i',3));	//3
		System.out.println(str.lastIndexOf('i',5));	//3
		
	
	}
}
