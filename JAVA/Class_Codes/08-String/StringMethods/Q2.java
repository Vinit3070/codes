/*

	Take two strings from user and and compare contents of those strings and state whether those are equal or not
		if equal-0
		else -print difference
*/
import java.util.*;
class Solution{

	static int myStrCmp(String str1,String str2){
		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();
		int sub=0;
		if(arr1.length==arr2.length){
			for(int i=0;i<arr1.length;i++){
				if(arr1[i]==arr2[i]){
					sub=arr1[i]-arr2[i];
					break;
				}
			}
		}
		else{
			sub=arr1.length-arr2.length;
		}
		return sub;
	}	

	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Two Strings::");
		
		String str1=sc.next();
		String str2=sc.next();
	
		int val=myStrCmp(str1,str2);
		System.out.println("OP : "+val);
		
	}
}

