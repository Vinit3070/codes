//charAt()
//It returns character indexes 
class Solution{
	public static void main(String[] args){
		String str1="Core2Web";

		System.out.println(str1.charAt(0));		//C
		System.out.println(str1.charAt(3));		//e
		System.out.println(str1.charAt(7));		//b
	}
}
