//Concat Method

class Solution{
	public static void main(String[] args){
		String str1="Vinit";
		String str2="Nikam";

		String str3=str1.concat(str2);		//Calls String concat() method

		System.out.println(str1);		//Vinit
		System.out.println(str2);		//Nikam
		System.out.println(str3);		//VinitNikam
	}
}
