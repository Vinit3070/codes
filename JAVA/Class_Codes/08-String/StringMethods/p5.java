//compareToIgnoreCase()-
//
//returns 0 if both are same
//
class Solution{
	public static void main(String[] args){
		String str1="Vinit";
		String str2="VinitNikam";			//5 letters are more in str2 than str1

		System.out.println(str1.compareToIgnoreCase(str2));	//-5
	}
}
