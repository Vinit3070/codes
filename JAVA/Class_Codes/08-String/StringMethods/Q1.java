/*	
	Wap that takes two strings from the user and check the length of those strings and state whether those strings are equal or not..!!
*/
import java.util.*;

class Solution{

	static int myStrLen(String str){
		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}

	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Two strings::");
		String str1=sc.next();
		String str2=sc.next();

		int str1Len=myStrLen(str1);
		int str2Len=myStrLen(str2);

		if(str1Len==str2Len){
			System.out.println((str1)+" & "+(str2)+ " are Equal");

		}else{
			
			System.out.println((str1)+" & "+(str2)+ " are Not Equal");
		}
	}
}
