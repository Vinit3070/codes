//String Locations as per strings at internals

class Solution{
	public static void main(String[] args){
		String str1="Vinit";			//SCP
		String str2="Vinit";			//SCP
		String str3=new String("Vinit");	//Heap
		String str4=new String("Vinit");	//Heap
		String str5=new String("Nikam");	//Heap
		String str6="Nikam";			//SCP

		int arr[]={1,2,3};


		System.out.println(System.identityHashCode(arr));
		
		System.out.println("String1: "+System.identityHashCode(str1));
		System.out.println("String2: "+System.identityHashCode(str2));
		System.out.println("String3: "+System.identityHashCode(str3));
		System.out.println("String4: "+System.identityHashCode(str4));
		System.out.println("String5: "+System.identityHashCode(str5));
		System.out.println("String6: "+System.identityHashCode(str6));
	}
}
