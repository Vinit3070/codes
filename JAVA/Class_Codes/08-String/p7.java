//String Assign and then Check Identity Hashcodes and its positions in internals

class Solution{
	public static void main(String[] args){
		
		String str1="Vinit";			//SCP
		String str2=str1;			//Assign Str1 --->> str2
		String str3= new String("Vinit");	//heap
							//
		System.out.println(System.identityHashCode(str1));	//1
		System.out.println(System.identityHashCode(str2));	//2
									//Both have Same IdentityHashCodes
		
		System.out.println(System.identityHashCode(str3));	//
	
	}
}
