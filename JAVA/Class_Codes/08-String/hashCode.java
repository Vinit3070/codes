//HashCode in Java
//
//it works on content

class Solution{
	public static void main(String[] args){

		//String str1="Vinit";
		//String str2=new String("Vinit");
		//String str3="Vinit";
		//String str4=new String("Vinit");

		String str1="Shashi";
		String str2=new String("Shashi");
		String str3="Shashi";
		String str4=new String("Shashi");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());

		//All give Same Hashcodes , Because all have same content in string.
	}
}
