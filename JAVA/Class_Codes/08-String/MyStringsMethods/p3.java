//mycharAt()-
//
class Solution{

	char mycharAt(String str,int index){
		
		char ch[]= str.toCharArray();
		
		return ch[index];

	}
	public static void main(String[] args){
		
		Solution obj=new Solution();

		String str="Vinit";

		System.out.println(obj.mycharAt(str,0));
		System.out.println(obj.mycharAt(str,3));
	
	}
}
