//My String Length
//
//Count Length using Our Function

class Solution{

	int myStrLength(String str){

		int count=0;

		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){
			count++;
		}

		return count;
	
	}
	public static void main(String[] args){
		
		Solution obj=new Solution();
		
		String str="Vinit";

		System.out.println("Count is:"+obj.myStrLength(str));

	

	}
}
