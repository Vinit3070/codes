//Parameterized Constructor
class Solution{
	
	int x=10;

	Solution(int a){				//Parameterized Constructor = Solution(Solution this, int a)
		System.out.println("X= "+x);
		System.out.println("A= "+a);
		
	}
	public static void main(String[] args){
		
		Solution obj=new Solution(15);		//Solution(Solution obj,15)

	}
}
