class Solution{
	
	int x=10;
	static int y=20;

	static void gun(){
		//System.out.println("gunX= "+x);
		System.out.println("gunY= "+y);				//Static in static 
	}
	void fun(){
		System.out.println("funX= "+x);				//10
		System.out.println("funY= "+y);				//20
	}
	public static void main(String[] args){
		Solution obj= new Solution();
		System.out.println("Non Static X= "+obj.x);
		System.out.println("static Y= "+y);			//Static in static 
		System.out.println("Class Y= "+Solution.y);		//Static using class name 
		
		Solution.gun();				
		obj.fun();
	}
}
