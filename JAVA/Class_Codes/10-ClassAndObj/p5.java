// CONSTRUCTOR - There are two types of constructors in java 
// 		1. No Argument
// 		2. Parameterized

class Solution{

	int x=10;

	Solution(){						//No argument Constructor 
		System.out.println("In Constructor..!!!");
		System.out.println(x);
	}
	
	public static void main(String[] args){
		Solution obj=new Solution();	
	}
}
