
class Solution{
	int x=10;			//Instance Variable
	static int y=20;		//Static Variable
	public static void main(String[] args){
		Solution obj=new Solution();		//Object Creation
		System.out.println("X= "+obj.x);	// 10	Access Non-static variable using object
		System.out.println("Y= "+y);		// 20
	}


}
