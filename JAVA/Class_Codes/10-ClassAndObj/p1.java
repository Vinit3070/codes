
//Simple Class Structure 

class Solution{

	int x=10;						//Instance / Global Variable	
	static void fun(){
		int y=20;					//Non-static Local Variable
		System.out.println("Y= "+y);	
	}
	public static void main(String[] args){
		int z=30;					//Non-static Local Variable
		//System.out.println("X= "+x);			//Error-Non static variable x cannot be referenced from the static context..	
		System.out.println("Z= "+z);


	}
}
