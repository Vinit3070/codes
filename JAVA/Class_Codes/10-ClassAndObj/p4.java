class Solution{
	int x=10;		//In Constructor

	static int y=20;	//In Static block

	void fun(){
		int z=30;	//In Function
	}

	public static void main(String[]args){
		
	}
}

/*
 * After Compilation Bytecode 
 
 	class Solution{
		int x;
		static int y;

		Solution():{
			invokespecial();
			x=10;			//instance var
		}
		static{
			y=20;			//bipush
		}

		fun(){
			z=30;			//bipush
		}
	}
*/
