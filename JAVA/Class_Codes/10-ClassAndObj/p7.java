class Solution{
	Solution(){
		System.out.println("In constructor");
	}

	void fun(){
		Solution obj=new Solution();

		System.out.println("OBJ= "+obj);
	}
	public static void main(String[] args){
		
		Solution obj1=new Solution();
		Solution obj2=new Solution();
	
		obj1.fun();
		System.out.println("OBJ1= "+obj1);
		System.out.println("OBJ2= "+obj2);


	}
}
