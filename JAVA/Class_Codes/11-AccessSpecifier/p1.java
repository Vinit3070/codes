//Realtime Example --

class Mumbai_Indians{
	
		int jerNo=45;					//Instance
		String p_Name="Rohit Sharma";			//Instance

		static String teamJerCol="Blue";		//Static 

		void p_Info(){
			System.out.println();
			System.out.println("\tPlayer's Jersey No= "+jerNo);
			System.out.println("\tPlayer's Name= "+p_Name);
			System.out.println("\tPlayer's Jersey Colour= "+teamJerCol);
			System.out.println();
		}
}
class Solution{
	public static void main(String[] args){
		Mumbai_Indians Mi1=new Mumbai_Indians();
		Mumbai_Indians Mi2=new Mumbai_Indians();

//Before the change in data..

		Mi1.p_Info();
		System.out.println("**********************************************************************");
		
		Mi2.p_Info();
		System.out.println("**********************************************************************");

//Change the Data
		
		Mi2.jerNo=63;
		Mi2.p_Name="Suryakumar Yadav";
		Mi2.teamJerCol="Blue_&_Gold";

		Mi1.p_Info();
		System.out.println("**********************************************************************");
		Mi2.p_Info();
		System.out.println("**********************************************************************");
	}
}
