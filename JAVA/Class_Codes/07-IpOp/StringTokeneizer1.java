
import java.io.*;
import java.util.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Match Info, MOM player and Runs");

		String info =br.readLine();

		StringTokenizer st=new StringTokenizer(info," ");		// It can Spearate data using (blankspaces)
										// if we got blankspace then it assigns next data to next tokem

		String tkn1= st.nextToken();
		String tkn2= st.nextToken();
		String tkn3= st.nextToken();

		System.out.println("Match Info::"+tkn1);
		System.out.println("MOM::"+tkn2);
		System.out.println("Runs::"+tkn3);

	}
}
