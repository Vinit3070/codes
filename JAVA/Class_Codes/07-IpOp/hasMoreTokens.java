//Count no of tokens in particular input given by user
//
import java.io.*;
import java.util.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		Scanner sc= new Scanner(System.in);

		System.out.print("Enter Player Info::");

		String str=sc.nextLine();

		StringTokenizer st=new StringTokenizer(str," ");

		System.out.println(st.countTokens());
		
		while(st.hasMoreTokens()){
			System.out.println(st.nextToken());
		}
	}
}
