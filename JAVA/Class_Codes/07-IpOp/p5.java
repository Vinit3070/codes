//Using BufferedReader class we can overcome limitations of InputStreamReader class.
//BufferedReader can accpet all types of data and print it same like IP.
//
import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br= new BufferedReader(isr);

		//System.out.println("ENTER DATA::");
		//char ch=(char)isr.read();

		//System.out.println(ch);				//It can takes only single character eg.,Vinit (V)


		System.out.print("ENTER DATA::");
		String ch2=br.readLine();

		System.out.print("The IP is::"+ch2);			//It prints Whole World with blanckspaces ....

		System.out.println();
	}
}
