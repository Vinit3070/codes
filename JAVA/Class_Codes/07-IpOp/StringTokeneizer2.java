// Real time Example for Taking input from user with StringTokeneizer class .
// take String ,int,char and float...

import java.io.*;
import java.util.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.print("Enter PlayerName , JerseyNo, GradeOfPlyaer and His Batting Average :: ");

		String str= br.readLine();

		StringTokenizer st= new StringTokenizer(str," ");

		String pName= st.nextToken();
		int jerNo=Integer.parseInt(st.nextToken());
		char gradeOfPlayer=st.nextToken().charAt(0);
		float avg=Float.parseFloat(st.nextToken());

		System.out.println();

		System.out.println("Player Name is:: "+pName);
		System.out.println("Player JerseyNo is:: "+jerNo);
		System.out.println("Player's Grade is :: "+gradeOfPlayer);
		System.out.println("Player's Average is :: "+avg);
	}
}
