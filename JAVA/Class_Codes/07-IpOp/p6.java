//Taking Integer values from user

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);

		//int age=br.readLine();		//Incompatible types : String can't be converted to int
		System.out.print("Enter Age::");
		int age =Integer.parseInt(br.readLine());

		System.out.println("Age is::"+age);
	}
}
