//Scanner class

import java.util.Scanner;		//Without package it cant run properly
					//It Gives us error of Cannot find Symbol for Scanner keywords/class

class Solution{
	public static void main(String[] args){
		
		Scanner obj= new Scanner(System.in);

		System.out.print("Enter the String Name::");
		
		String name=obj.next();

		System.out.println("String::"+name);	//it only takes a string before whitespaces.
							//it dont consider string after whitespaces....
							//
							//Eg- Vinit Nikam
							//OP- Vinit
	}
}
