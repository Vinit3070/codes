//Bitwise Operators << >> 

class Bitwise3{
	public static void main(String[] args){
		int x=5;			//0000 0101
		int y=9;			//0000 1001

		System.out.println(x<<2);       //0001 0100	//20
		System.out.println(y>>2);       //0000 0010	//2
	}
}
