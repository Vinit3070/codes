//Relational Operators with if statement 

class Relational2{
	public static void main(String[] args){
		int x=10;
		int y=20;

		//if(x)	//error - Incompatible types int cannot be converted into boolean
		//if(1)	//same error
		
		if(false)
			System.out.println("HELLO..!");
		else
			System.out.println("BYEE..!!");
	}
}
