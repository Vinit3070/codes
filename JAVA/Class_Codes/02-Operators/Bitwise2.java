//Bitwise & | ^

class Bitwise2{
	public static void main(String[] args){
		int x=9;		//0000 1001
		int y=17;		//0001 0001
					
		System.out.println(x&y);	//1
		System.out.println(x|y);	//25
		System.out.println(x^y);	//24
	}
}
/*
 
  & 	0000 1001
	0001 0001
	---------
	0000 0001	=1

	
  | 	0000 1001		
	0001 0001
	---------
	0001 1001	=25


  ^	0000 1001
 	0001 0001
	---------
	0001 1000	=24
*/	
