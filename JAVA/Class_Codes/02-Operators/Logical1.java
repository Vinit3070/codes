//Logical Operators && ||

class Logical1{
	public static void main(String[] args){
		
		int x=2;
		int y=5;

		boolean ans1= x<y && y<x;		//false
		boolean ans2= x<y && y>x;		//true
		boolean ans3= x<y || y<x;		//true
		boolean ans4= x>y || y<x;		//false

		System.out.println(ans1);
		System.out.println(ans2);
		System.out.println(ans3);
		System.out.println(ans4);


	}
}

/*
 	In java it only need boolean values for logical operators comparison.
	No other type of datatypes accepted by Java.
*/
