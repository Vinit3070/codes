//Simple Increment and Decrement Operations

class Unary1{
	public static void main(String[] args){
		
		int a=10;
		int b=15;

		System.out.println(a++);		//10
		System.out.println(b++);		//15
		System.out.println(a);			//11
		System.out.println(b);			//16
	}
}
