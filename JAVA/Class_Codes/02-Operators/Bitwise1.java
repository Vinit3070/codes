//Bitwise Operators- & | ^ << >> >>> ~

class Bitwise1{
	public static void main(String[] args){

		int x=23;	//0001 0111
		int y=14;	//0000 1110

		System.out.println(x&y);	//6		//0001 0111
								//0000 1110
								//---------
								//0000 0110   ====> 6
	
		System.out.println(x|y);	//31		//0001 0111
								//0000 1110
								//---------
								//0001 1111   ====> 31
	}
}
