//Simple Relational Operations..

class Relational{
	public static void main(String[] args){
		int x=50;
		int y=20;

		System.out.println(x<y);		//false
		System.out.println(x>y);		//true
		System.out.println(x<=y);		//false
		System.out.println(x>=y);		//true
		System.out.println(x==y);		//false
		System.out.println(x!=y);		//true
	
	}
}
