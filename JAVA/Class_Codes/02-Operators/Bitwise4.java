//Bitwise Operators << >> >>>

class Bitwise4{
	public static void main(String[] args){
		int x=14;			//0000 1110
		int y=7;			//0000 0111
		int z=-7;

		System.out.println(x<<3); 	        //0111 0000	//114
		System.out.println(y>>2);       	//0000 0001	//1
		System.out.println(z>>>31);       	//0000 0001     //1 
							
		//It shifts all bit homes upto 31 th position so there is only 1th position is remaining so ans is 1.
	}
}
