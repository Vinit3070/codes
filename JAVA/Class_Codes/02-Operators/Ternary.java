//Ternary operators in java 
// ? :

class Ternary{
	public static void main(String[] args){
	 	int x=10;
		int y=20;

		System.out.println((x<y)? x:y);		//10
							//It just works like if else statements
							//if x is smaller than y then it prints x, otherwise it prints y...
	}
}
