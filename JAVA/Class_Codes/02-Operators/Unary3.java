//Unary Operations

class Unary3{
	public static void main(String[] args){
		int x=10;
		int y=20;

		int ans1= ++x + x++;		//12
		int ans2= y-- + --y;		//18
		
		System.out.println(x);
		System.out.println(y);

	}
}
