//Unary Operation

class Unary2{
	public static void main(String[] args){
		int x=5;
		int y=9;

		System.out.println(x++);		//5
		System.out.println(y++);		//9
		
		System.out.println(x);			//6
		System.out.println(y);			//10
		
		System.out.println(x--);		//6
		System.out.println(y--);		//10

		System.out.println(x);			//5
		System.out.println(y);			//9
	}
}
