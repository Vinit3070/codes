//Unary Operations..

class Unary4{
	public static void main(String[] args){
		
		int x=8;
		int y=5;
							
		System.out.println(x);			//8
		System.out.println(y);			//5


		int ans1= x++ + y-- + ++x;		
		int ans2= x++ + --y + ++x;		
									
		System.out.println(ans1);		//23
		System.out.println(ans2);		//25
		System.out.println(x);			//12
		System.out.println(y);			//3

	}
}
