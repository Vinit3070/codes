//List Interface 

/*
	-In List Interface Duplicate Date is Allowed
	-Insertion Order is preserved 

	CLASSES- 1. ArrayList
		 2. LinkedList
		 3. Vector 
			- Stack
*/

//ArrayList-

import java.util.*;

class ArrayListDemo extends ArrayList{
	
	public static void main(String[] args){
		
		ArrayListDemo obj= new ArrayListDemo();

		obj.add(10);
		obj.add(20);
		obj.add(50);
		obj.add(40);
		obj.add(30);

		System.out.println(obj);
	}
}
