//User Defined ArrayList

import java.util.*;

class Player{

	String pName;
	int jerNo;
	double avg;

	Player(int jerNo,String pName,double avg){
		
		this.jerNo=jerNo;
		this.pName=pName;
		this.avg=avg;
	}

	public String toString(){
		return jerNo+" : "+pName+" : "+avg;
		
	}
	
}
class Solution{
	public static void main(String[] args){
		
		ArrayList obj=new ArrayList();

		obj.add(new Player(18,"Virat",53.60));
		obj.add(new Player(45,"Rohit",59.50));
		obj.add(new Player(63,"Surya",45.60));

		System.out.println(obj);
			
		System.out.println(obj.remove(2));			//removes Surya 's Entry
		System.out.println(obj);				//[virat,rohit]
			

	}
}
