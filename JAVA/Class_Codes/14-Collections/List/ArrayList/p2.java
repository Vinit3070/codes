//Arraylist Methods

import java.util.*;
class Solution extends ArrayList{

	public static void main(String[] args){
		
		Solution obj=new Solution();

		obj.add(4);
		obj.add(1);
		obj.add("Vinit");
		obj.add("Rushi");
		obj.add(7);

		System.out.println(obj);			//[4,1,7]

	//size()
		System.out.println(obj.size());			//3
	//contains()
		System.out.println(obj.contains(4));		//true
		System.out.println(obj.contains("Vinit"));	//true
	//indexOf()
		System.out.println(obj.indexOf("Rushi"));	//3
	//lastIndexOf()
		System.out.println(obj.lastIndexOf("Vinit"));	//2
	//get()
		System.out.println(obj.get(4));			//7
	//set()
		System.out.println(obj.set(4,"Pavan"));		//If Index is Present then the new data is replaced with already present data.
								//If we insert data and list limit exceeds then IndexOutOfBoundsException throw.	
		System.out.println(obj);			//[4,1,Vinit,Rushi,Pavan]    		Pavan added over 7 which is at 4th index 
	//add()
		obj.add(5,7);		
		System.out.println(obj);			//[4,1,Vinit,Rushi,Pavan,7]
	//remove()
		System.out.println(obj.remove(1));		//1 is removed from the list
		System.out.println(obj);			//[4,Vinit,Rushi,Pavan.7]
	//addAll()
	
		Solution obj2 =new Solution();
		obj2.add("Gta-V");
		obj2.add("COD");
		obj2.add("BGMI");

		obj.addAll(obj2);				//Method adds new list elements into existing list
		System.out.println(obj);			//[4,Vinit,Rushi,Pavan,7,Gta-V,COD,BGMI]
	//addAll(int,Collection)
	//This method can add The list at our entered index
	//suppose- 3= From 3 rd index the list in added and later on existing list is continued as it is.
		obj.addAll(4,obj2);			
		System.out.println(obj);			//[4,Vinit,Rushi,Pavan,Gta-V,COD,BGMI,7,Gta-V,COD,BGMI]
								
	//removeRange(int,int)
	
		obj.removeRange(4,7);
		System.out.println(obj);
	
	//Object[] toArray();
	
		Object arr[] =obj.toArray();
		for(Object data : arr){
			System.out.print(data+" ");
		}

		System.out.println();


	}
}
