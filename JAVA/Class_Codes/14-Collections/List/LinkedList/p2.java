//LinkedList Methods

import java.util.*;
class Solution{

	public static void main(String[] args){
		LinkedList obj=new LinkedList();

		obj.add(5);
		obj.addFirst(10);
		obj.add(30);
		obj.addLast(50);

		System.out.println("Before Offer");
		System.out.println(obj);
		
//offer
		System.out.println("After Offer");
		obj.offer(60);				//adds element in the list
		obj.offerFirst(1);			//adds element in the list
		obj.offerLast(90);			//adds element in the list
		System.out.println(obj);		//[1,10,5,30,50,60,90]
//getFirst and getLast
		
		System.out.println("getFirst and getLast");
		System.out.println(obj.getFirst());	//1
		System.out.println(obj.getLast());	//90
		
//peek	

		System.out.println("peek");
		//System.out.println(obj.peek());	//1	it also prints first
		System.out.println(obj.peekFirst());	//1
		System.out.println(obj.peekLast());	//90
//poll	
		
		System.out.println("poll");
		//System.out.println(obj.poll());	//1
		System.out.println(obj.pollFirst());	//10
		System.out.println(obj.pollLast());	//90
		System.out.println(obj);		//[10,5,30,50,60]
//push

		obj.push(100);				//100
		System.out.println(obj);		//[100,10,5,30,50,60]
//pop		
		System.out.println(obj.pop());		//100 ---->>>  [10,5,30,50,60]
//remove

		System.out.println(obj.remove(3));	//50
		System.out.println(obj);		//[10,5,30,60]

//element
		System.out.println(obj.element());	//First Data is Printed --->>10
		
	}
}
