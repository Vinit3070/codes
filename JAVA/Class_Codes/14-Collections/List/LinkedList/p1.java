//LinkedList
import java.util.*;
class Solution {

	public static void main(String[] args){
		
		LinkedList obj=new LinkedList();
		/*obj.add(10);
		obj.add(40);
		obj.add(5);
		obj.add(70);
		obj.add(13);*/

		obj.add(30);
		obj.addFirst(5);
		obj.addLast(40);

		System.out.println(obj);				//[5,30,40]

		System.out.println("First-"+obj.getFirst());		//5
		System.out.println("Last-"+obj.getLast());		//40
	}
	
}
