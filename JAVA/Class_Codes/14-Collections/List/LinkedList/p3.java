

import java.util.*;

class Player {

	int jerNo;
	String name;
	Player (int jerNo,String name){
		this.jerNo=jerNo;
		this.name=name;
	}
	public String toString(){
		return jerNo+" : "+name;
	}
}
class Solution{

	public static void main(String[] args){
		
		LinkedList obj=new LinkedList();

		obj.add(new Player(18,"Virat"));
		obj.add(new Player(45,"Rohit"));
		obj.add(new Player(7,"Msd"));
		obj.add(new Player(1,"KLRahul"));
		obj.add(new Player(63,"Surya"));

		System.out.println(obj);

		obj.offer(new Player(32,"Ishan Kishan"));
		System.out.println(obj);
		
		obj.push(new Player(8,"Jadeja"));
		System.out.println(obj);

		obj.pop();
		System.out.println(obj);

		System.out.println(obj.element());
	}
}
