/*
	SET- Set is the Container which is used to store data which is not duplicate.
		i.e, DUPLICATE DATA IS NOT ALLOWED

	TYPES-	
	
	CLASSES==					INTERFACES==
		1. HashSet						1.SortedSet
		2. LinkedHashSet					2.NavigableSet
		3. TreeSet
*/

//It works on Hash Values .


import java.util.*;

class Solution{
	
	public static void main(String[] args){
		
		HashSet hs=new HashSet();

		hs.add("RUSHI");
		hs.add("PAVAN");
		hs.add("VINIT");
		hs.add("NIKHIL");
		hs.add("SHUBHAM");
		hs.add("NIKHIL");

		//String str="VINIT";
		

		System.out.println(hs);	 		//[PAVAN,VINIT,RUSHI,NIKHIL,SHUBHAM]

	}
}

