//HashSet Methods

import java.util.*;
class Solution{

	public static void main(String[] args){
		
		HashSet hs=new HashSet();

		hs.add(30);	
		hs.add(10);	
		hs.add(20);	
		hs.add(40);	
		hs.add(50);
		System.out.println(hs);				//[50,20,40,30,10]

		System.out.println(hs.isEmpty());		//false
		
		System.out.println(hs.contains(30));		//true
		
		System.out.println(hs.add(60));			//true
		System.out.println(hs);				//[50,20,40,10,60,30]
							
		System.out.println(hs.remove(40));		//true
		System.out.println(hs);				//[50,20,10,60,30]
								
		hs.clear();					//This will remove all the elements from the set. 
		System.out.println(hs);				//[]	
			
	}
}

