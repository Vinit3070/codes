//Treeset

import java.util.*;
class Solution{
	public static void main(String[] args){
		
		TreeSet ts=new TreeSet();
		ts.add(40);
		ts.add(10);
		ts.add(30);
		ts.add(20);
		ts.add(40);
		ts.add(20);

		System.out.println(ts);		//TreeSet gives us sorted data , It also removes duplicate data
	}
}
