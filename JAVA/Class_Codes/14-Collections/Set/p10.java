//Treeset

import java.util.*;
class Solution{
	public static void main(String[] args){
		
		TreeSet ts=new TreeSet();
		ts.add("Vinit");
		ts.add("Virat");
		ts.add("Vivek");
		ts.add("Vivek");
		ts.add("Pavam");
		ts.add("Rushi");

		System.out.println(ts);		//TreeSet gives us sorted data , It also removes duplicate data
	}
}
