//HashSet Methods

import java.util.*;

class HashSetDemo implements Comparable{

	String name;
	
	HashSetDemo(String name){
		
		this.name=name;
	}

	public String toString(Object obj){
		
		return this.str.compareTo((HashSetDemo)obj.str);
	}
	


}
class Solution{

	public static void main(String[] args){
		
		HashSet hs=new HashSet();

		hs.add(new HashSetDemo("Pavan"));	
		hs.add(new HashSetDemo("Vinit"));	
		hs.add(new HashSetDemo("Rushi"));	
		hs.add(new HashSetDemo("Nikhil"));	
		hs.add(new HashSetDemo("Shubham"));	
	
		System.out.println(hs);				//[50,20,40,30,10]
	
	/*
		System.out.println(hs.isEmpty());		//false
		
		System.out.println(hs.contains(30));		//true
		
		System.out.println(hs.add(60));			//true
		System.out.println(hs);				//[50,20,40,10,60,30]
							
		System.out.println(hs.remove(40));		//true
		System.out.println(hs);				//[50,20,10,60,30]
								
		hs.clear();					//This will remove all the elements from the set. 
		System.out.println(hs);				//[]	
	*/

	}
}

