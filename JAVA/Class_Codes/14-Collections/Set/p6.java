//Linked HashSet

import java.util.*;
class Solution {
	public static void main(String[] args){
	
		LinkedHashSet lhs=new LinkedHashSet();
		lhs.add(10);
		lhs.add(20);

		lhs.add(new Integer(10));
		lhs.add(new Integer(30));

		System.out.println(lhs);
	}
}

//add(new Integer(10)) == add(10)
//
//goes same internally and if same then return only once 
