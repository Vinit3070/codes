//Vector 
//
//It is SYNCHRONISED i.e Its all methods are Synchronised

import java.util.*;
class Solution{
	
	public static void main(String[] args){
		Vector obj= new Vector();

		obj.addElement(10);
		obj.addElement(1);
		obj.addElement(15);
		obj.addElement(13);

		System.out.println(obj);			//[10,1,15,13]
		
		System.out.println(obj.size());			//4
		
		System.out.println(obj.contains(2));		//false
		
		System.out.println(obj.elementAt(2));
		
		obj.add(60);
		System.out.println(obj);			//[10,1,15,13,60]

		obj.remove(1);				
		System.out.println(obj);			//[10,15,13,60]		
		



	}
}
