//Vector METHODS

import java.util.*;
class Solution{
	
	public static void main(String[] args){
		Vector obj= new Vector();

		obj.addElement(10);
		obj.addElement(1);
		obj.addElement(15);
		obj.addElement(13);

		System.out.println(obj);			//[10,1,15,13]
		
		System.out.println(obj.size());			//4
		
		System.out.println(obj.contains(2));		//false
		
		System.out.println(obj.elementAt(2));		//15
		System.out.println(obj.firstElement());		//10
		System.out.println(obj.lastElement());		//13
		
		obj.add(60);
		System.out.println(obj);			//[10,1,15,13,60]

		obj.remove(1);				
		System.out.println(obj);			//[10,15,13,60]		
		
		System.out.println(obj.indexOf(13));		//2
		System.out.println(obj.lastIndexOf(15));	//1
		System.out.println(obj.lastIndexOf(40));	//-1	If not in Vector List then it gives -1
		
		System.out.println(obj.get(3));			//60
		obj.set(2,40);
		System.out.println(obj);			//[10,15,40,60]
		
		obj.setElementAt(100,2);
		System.out.println(obj);			//[10,15,100,60]
		obj.insertElementAt(20,3);
		System.out.println(obj);			//[10,15,100,20,60]
		obj.removeElementAt(1);			
		System.out.println(obj);			//[10,100,20,60]
		
		ListIterator it=obj.listIterator();
		while(it.hasNext()){
			System.out.println(it.next());		//[10,100,20,60]
		}



	}
}
