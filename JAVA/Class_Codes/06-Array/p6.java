import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		int arr[]=new int[5];

		System.out.println("ENTER ARRAY ELEMENTS::");
		for(int i=0;i<5;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("ARRAY ELEMENTS ARE::");

		for(int i=0;i<5;i++){
			System.out.print(arr[i]+" ");
		}
	}
}	

//NOTE-
//If we give input like char, float and other types of data then
//it gives us ERROR of NUMBERFORMATEXCEPTION.
