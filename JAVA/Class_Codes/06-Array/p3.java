// ARRAY DECLARATION AND ASSIGNMENT-

class Solution{
	public static void main(String[] args){
	
		/*arr[0]=20;
		arr[1]=20;
		arr[2]=20;
		arr[3]=20;
		arr[4]=20;
		System.out.println("INT::");
		for(int i=0;i<5;i++){
			System.out.println(arr[i]);
		}

		char arr1[]=new char[5];
		arr1[0]='A';
		arr1[4]='E';
		System.out.println("CHAR::");

		for(int i=0;i<5;i++){
			System.out.print(arr1[i]);
		}*/

		
		int arr1[]={10,20,30};
		float arr2[]={10.5f,20.5f,30.5f};
		char arr3[]={'A','B','C'};
		double arr4[]={10.50,20.50,30.50};
		boolean arr5[]={false,true,false};
		
		System.out.print("INT::");
		for(int i=0;i<3;i++){
			System.out.print(arr1[i]+" ");
		}
		System.out.println();

		System.out.print("FLOAT::");
		for(int i=0;i<3;i++){
			System.out.print(arr2[i]+" ");
		}
		System.out.println();

		System.out.print("CHAR::");
		for(int i=0;i<3;i++){
			System.out.print(arr3[i]+" ");
		}
		System.out.println();

		System.out.print("DOUBLE::");
		for(int i=0;i<3;i++){
			System.out.print(arr4[i]+" ");
		}
		System.out.println();

		System.out.print("BOOLEAN::");
		for(int i=0;i<3;i++){
			System.out.print(arr5[i]+" ");
		}
		System.out.println();





	}
}
