//Simple Array Concepts
//Array is collection of similat types of data..!!
//
class Solution{
	public static void main(String[] args){
		
		//int arr[];			// Error- Invalid Declaration in java
		//int arr[]= new int[];		// Error- Array Dimensions Missing

		int arr[]= new int[5];		// NO ERROR

		int arr1[]= {10,10,10};			//NO ERROR
		int arr2[]= new int[]{10,10,10};	//NO ERROR
		//int arr3[]= new int[3]{10,10,10};	//ARRAY DIMENSIONS AND INITIALISATION NOT SHOULD BE TAKEN TOGETHER ,IT IS ILLEGAL

		System.out.println("Hii ARRAY!!!");
	}
}
