//INTEGER CACHE
//If it is in this range (-128 to 127) Then---> It Makes a POOL and This Pool is Known as INTEGER CACHE
//
//for ex..// If the var having same value then both having same IdentityHashCode So it goes on INTEGER CACHE

class Solution{
	public static void main(String[] args){
		
		int x=10;
		int y=20;

		System.out.println(System.identityHashCode(x));		//Different
		System.out.println(System.identityHashCode(y));		//Different

		int a=100;
		int b=100;
		System.out.println(System.identityHashCode(a));		//Same
		System.out.println(System.identityHashCode(b));		//Same

	}
}
