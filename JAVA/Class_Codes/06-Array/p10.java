class Solution{
	public static void main(String[] args){
		
		int arr1[]={100,200,300,400};
		byte arr2[]={1,2,3};
		short arr3[]={10,20,30,40};
		boolean arr4[]={true,false,true};
		float arr5[]={10.5f,20.2f,30.2f,40.3f};
		double arr6[]={10.45,20.45,30.55,40.55};
		double arr7[]={10.45d,20.45d,30.55d,40.55d};
		//float arr8[]={10.5,20.5,30.5,40.5};

		System.out.println(arr1);		//Integer Address
		System.out.println(arr2);		//Byte Address
		System.out.println(arr3);		//Short Address
		System.out.println(arr4);		//Boolean Address
		System.out.println(arr5);		//Float Address
		System.out.println(arr6);		//Double Address
		System.out.println(arr7);		//It also Return Address
		//System.out.println(arr8);		//Error-Incompatible types(PLC)from Double to Float..!!  
		//
		char arr9[]={'A','B','C'};
		String str="Vinit Nikam";
		char arr10[]=new char[5];

		System.out.println(arr9);		//ABC
		System.out.println(str);		//Vinit Nikam
		System.out.println(arr10);		//(Blank)
		
	}
}
