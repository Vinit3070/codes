
//Take array element from the user and print the count of even elements;

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an array::");
		int size= Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.print("Array Elements Are::");
		for(int i=0;i<arr.length;i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			
		}
		for(int i=0;i<arr.length;i++){
			
			System.out.print(arr[i]+" ");

		}
			
		System.out.println();
		
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				count++;
			}
		}

		System.out.println("Count of Even Array Elemnets is::"+count);
	}
}
