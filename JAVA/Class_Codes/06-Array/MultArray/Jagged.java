//Simple Jagged Array(2D)


import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{

		
		int arr[][]=new int[3][];			//Rows are Compulsary
								
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		for(int i=0;i<arr.length;i++){
			System.out.print("Enter the Size of Columns do you want at row " +i+ "::");
			int size=Integer.parseInt(br.readLine());
			arr[i]= new int[size];
		
			
		System.out.println("Enter Array Elements for Row "+i+" :: ");
	
		for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}

		
		}
	

		System.out.println("Array Elements are::");
			
				
		
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");		//10 20 30 
										//40 50 60
										//70 80 90
			}
			System.out.println();
		}

	}
}

