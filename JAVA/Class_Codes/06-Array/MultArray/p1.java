//Simple 2D-Array

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{

//1st Way of declaration		
		int arr[][]=new int[3][];			//Rows are Compulsary
								
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		arr[0]=new int[3];
		arr[1]=new int[3];
		arr[2]=new int[3];

		System.out.println("Enter Array Elements::");
	
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}

		
		}

		System.out.println("Array Elements are::");
			
				
		
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");		//10 20 30 
										//40 50 60
										//70 80 90
			}
			System.out.println();
		}
	}
}
