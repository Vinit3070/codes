// ARRAY DECLARATION TYPES 
//
// DEFAULT VALUES WITH EACH DATATYPES-
// 	1 . int --> 0
// 	2 . float --> 0.0
// 	3 . char --> (Blank Character)
// 	4 . boolean --> false
// 	4 . double --> 0.0
// 	4 . long --> 0
// 	4 . short --> 0

class Solution{
	public static void main(String[] args){
		
		int arr1[] = new int[3];
		float arr2[] = new float[3];
		char arr3[] = new char[3];
		boolean arr4[] = new boolean[3];
		
		System.out.print("INT:: ");
		for (int i=0;i<3;i++){
			System.out.print(arr1[i]+" ");				// 0  0  0
		}
		System.out.println();


		System.out.print("FLOAT:: ");
		for (int i=0;i<3;i++){
			System.out.print(arr2[i]+" ");				//0.0  0.0  0.0
		}
		System.out.println();


		System.out.print("CHAR:: ");
		for (int i=0;i<3;i++){
			System.out.print(arr3[i]+" ");				// 3 blank characters   
		}
		System.out.println();


		System.out.print("BOOLEAN:: ");
		for (int i=0;i<3;i++){
			System.out.print(arr4[i]+" ");				//false false false
		}
		System.out.println();



		double arr5[]=new double[3];
		System.out.print("DOUBLE::");
		for(int i=0;i<3;i++){
			System.out.print(arr5[i]+"  ");				//0.0  0.0  0.0
		}
		System.out.println();


		long arr6[]=new long[3];
		System.out.print("LONG::");
		for(int i=0;i<3;i++){
			System.out.print(arr6[i]+"  "); 			//0  0  0
		}
		System.out.println();

		short arr7[]=new short[3];
		System.out.print("SHORT::");
		for(int i=0;i<3;i++){
			System.out.print(arr7[i]+"  ");				//0  0  0
		}
		System.out.println();





	}
}
