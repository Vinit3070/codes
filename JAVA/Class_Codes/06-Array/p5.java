// TAKE ARRAY ELEMENTS FROM USER AND PRINT IT..
//
import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		int arr[]=new int[5];
		System.out.println("ENTER ARRAY ELEMENTS::");

		for(int i=0;i<5;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("ARRAY ELEMENTS ARE::");
		for(int i=0;i<5;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
