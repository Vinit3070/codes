//Take array element from the user and print its sum;

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an array::");
		int size= Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.print("Array Elements Are::");
		for(int i=0;i<arr.length;i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			
		}
		for(int i=0;i<arr.length;i++){
			
			System.out.print(arr[i]+" ");

		}
			
		System.out.println();
		
		int sum=0;
		for(int i=0;i<arr.length;i++){
			sum=sum+arr[i];
		}

		System.out.println("Sum of Array Elemnets is::"+sum);
	}
}
