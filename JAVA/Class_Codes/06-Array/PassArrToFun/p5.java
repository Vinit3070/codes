
//Passing char  array to a function


class Solution{

	static void fun(boolean arr[]){
		System.out.print("IN FUN : ");
		for(int i=0;i<arr.length;i++ ){
			
			System.out.print(arr[i]+"  ");
		}

		System.out.println();
	
	}
	public static void main(String[] args){
		
		boolean arr[]={true,false,true};

		System.out.print("IN MAIN 1: ");
		for(boolean b:arr)
			System.out.print(b+"  ");
		
		System.out.println();

		fun(arr);		//Passing array to function as an argument
		
		System.out.println();
	}
}
