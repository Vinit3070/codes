//Passing float array to a function


class Solution{

	static void fun(float arr[]){
		System.out.print("IN FUN :	");
		for(int i=0;i<arr.length;i++ ){
			
			arr[i]=arr[i]*2;;

			System.out.print("\t"+arr[i]+" ");
		}

		System.out.println();
	
	}
	public static void main(String[] args){
		
		float arr[]={10.0f,20.0f,30.0f};

		System.out.print("IN Main 1:	");
		for(float f:arr)
			System.out.print("\t"+f+" ");
		
		System.out.println();

		fun(arr);		//Passing array to function as an argument
		
		System.out.print("IN Main2:	");
		for(float f:arr)
			System.out.print("\t"+f+" ");
		

		System.out.println();
	}
}
