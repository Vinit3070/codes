//Passing Array to function and change value from function

class Solution{

	static void fun(int []arr){
		arr[0]=50;

		System.out.println(arr);		//Same as per Main Methods Array Address..!!!
		System.out.print("IN FUN::");
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println();
	}
	public static void main(String[] args){
		
		int arr[]={10,20,30};
		System.out.println(arr);		//Address is same in [main & fun] methods..!!!

		System.out.print("IN MAIN::");
		for(int x:arr)
			System.out.print(x+" ");
		System.out.println();

		fun(arr);
		System.out.println();
	}
}
