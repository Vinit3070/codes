//Passing array to a function


class Solution{

	static void fun(int xarr[]){
		System.out.print("IN FUN:");
		for(int i=0;i<xarr.length;i++ ){
			
			xarr[i]=xarr[i]+10;

			System.out.print(xarr[i]+" ");
		}

		System.out.println();
	
	}
	public static void main(String[] args){
		
		int arr[]={10,20,30,40,50};

		System.out.print("IN Main1:");
		for(int x:arr)
			System.out.print(x+" ");
		
		System.out.println();

		fun(arr);		//Passing array to function as an argument
		
		System.out.print("IN Main2:");
		for(int x:arr)
			System.out.print(x+" ");
		

		System.out.println();
	}
}
