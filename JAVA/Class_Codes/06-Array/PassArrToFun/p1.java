//Passing array to a function


class Solution{

	static void fun(int xarr[]){
		System.out.println("IN FUN:");
		for(int i=0;i<xarr.length;i++ ){
			System.out.print(xarr[i]+" ");
		}

		System.out.println();
	
	}
	public static void main(String[] args){
		
		int arr[]={10,20,30,40,50};

		fun(arr);		//Passing array to function as an argument
		
		System.out.println("IN Main:");
		for(int x:arr)
			System.out.print(x+" ");
		

		System.out.println();
	}
}
