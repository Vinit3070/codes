//Passing char  array to a function


class Solution{

	static void fun(char arr[]){
		System.out.print("IN FUN :	");
		for(int i=0;i<arr.length;i++ ){
			
			//arr[i]=arr[i]*2;

			System.out.print("\t"+arr[i]+" ");
		}

		System.out.println();
	
	}
	public static void main(String[] args){
		
		char arr[]={'V','i','n','i','t'};

		System.out.print("IN MAIN 1:	");
		for(char c:arr)
			System.out.print("\t"+c+" ");
		
		System.out.println();

		fun(arr);		//Passing array to function as an argument
		
		System.out.println();
	}
}
