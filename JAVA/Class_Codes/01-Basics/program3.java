//WAP for addition of varibles of int datatypes..

class Samp1{
	public static void main(String [] args ){
	int var1=20;
	int var2=25;

	//System.out.println("var1=" +var1);		//20
	//System.out.println("var2=" +var2);		//25

	//var1=var1+var2;			//ERROR-- Incompatible types- possible loosy coversions from int to short
	

	System.out.println("var1=" +var1);			//20
	System.out.println("var2=" +var2);			//25
	System.out.println("var1+var2= " +(var1+var2));		//45

	}
}
