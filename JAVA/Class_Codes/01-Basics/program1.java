//WAP for Datatypes implementation in java...

class Demo{
	public static void main(String [] args){
	
		int var1=10 ;				//size is 4 byte
		float f1=15.5f ;			//size is 4 byte
		short var2= 15 ;			//size is 2 byte
		char ch='A';				//size is 2 byte
		double var3= 10.554545;			//size is 8 byte

		System.out.println(var1);		//10
		System.out.println(f1);			//15.5
		System.out.println(var2);		//15
		System.out.println(ch);			//A
		System.out.println(var3);		//10.554545


	}

}
