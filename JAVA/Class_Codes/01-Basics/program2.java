//WAP for addition of varibles of short datatypes..

class Samp{
	public static void main(String [] args ){
	short var1=10;
	short var2=15;

	//System.out.println("var1=" +var1);		//10
	//System.out.println("var2=" +var2);		//15

	//var1=var1+var2;			//ERROR-- Incompatible types- possible loosy coversions from int to short
	

	System.out.println("var1=" +var1);			//10
	System.out.println("var2=" +var2);			//15
	System.out.println("var1+var2= " +(var1+var2));		//25

	}
}
