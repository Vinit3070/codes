/*
	PS-
	Take an Integer as input and print whether it is divisible by 4 or not.
	
	IP-5
	OP-Number is Not Divisible by 4

*/
class Solution{
	public static void main(String[] args){
		
		int no=12;

		if(no%4==0){
			System.out.println("Number is Divisible by 4..!!");
		}else{
			System.out.println("Number is Not Divisible by 4..!!");
		}
	}
}
