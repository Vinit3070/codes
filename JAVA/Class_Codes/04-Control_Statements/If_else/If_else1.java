//PS-
//Print the max of two
//Assume x=5
//	 y=7
//OP- 7 is greater

class MinMax{
	public static void main(String[] args){
		int x=5;
		int y=7;

		 if (x<y)
			System.out.println(y + " is greater");
		 else
			System.out.println(x + " is greater");
	}
}
