/*
 	PS-
	Given an integer input A which represents units of electricity consumed at your house .
	-Calculate and print the bill amount 
	-units <=100 --> price per unit is 1
	-units >100 --> price per unit is 2

*/
class Solution{
	public static void main(String[] args){
		int units=300;
		int bill=0;

		if(units<=100){
			System.out.println(units);
		}else{
			bill=units*2-100;
			System.out.println(bill);
		}
	}
}
