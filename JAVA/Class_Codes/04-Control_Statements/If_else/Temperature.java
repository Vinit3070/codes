/*
	PS- Given the Temperature of a person in Farenheit.
       		Print whether the person has high ,normal and low body temperature.
	1. > 98.6-->High
	2. 98.0 >= and <= 98.6	--> Normal
	3. < 98.0-->Low

*/
class Solution{
	public static void main(String[] args){
		float temp=98.8f;

		if (temp >= 98.6f){
			System.out.println("Temperature is High..!!");
		}
		else if(temp <= 98.0f){
			System.out.println("Temperature is Low...!!");
		}
		else{
			System.out.println("Temperature is Normal..!!");
		}
	}
}
