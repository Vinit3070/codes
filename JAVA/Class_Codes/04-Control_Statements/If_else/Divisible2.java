/*
	PS-
	Given and integer value as an input 
	-print fizz if it is divisible by 3
	-print buzz if it is divisible by 5
	-print fizz buzz if it is divisible by both 3 and 5
	-print " Not Divisible by both " if it is not divisible by both

*/
class Solution{
	public static void main(String[] args){
	
		int no=30;

		if(no%3==0 && no%5==0){
			System.out.println("FIZZ BUZZ");
		}else if(no%3==0){		
			System.out.println("FIZZ");
		}else if(no%5==0){
			System.out.println("BUZZ");
		}else{
			System.out.println("NOT DIVISIBLE BY BOTH");
		}
			
	}
}
