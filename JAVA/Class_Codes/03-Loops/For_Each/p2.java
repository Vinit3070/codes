
//Print the Array with For each Loop
//
class Solution{
	public static void main(String[] args){
	
		int arr[]={10,20,30,40,50};

		//For Loop
		System.out.println("FOR");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		
		System.out.println();

		//for-each
		System.out.print("For Each");

		for(float x:arr)
			System.out.print(x+" ");
		
		System.out.println();
	}
}
