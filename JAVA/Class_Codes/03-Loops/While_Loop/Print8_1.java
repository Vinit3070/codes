//Print integers from 8 to 1 using while Loop

class Solution{
	public static void main(String[] args){
		int i=8;

		while(i>=1){
			//System.out.print(" "+i--);
			System.out.print(" " +i);
			i--;
		}
		System.out.println();
	}
}
