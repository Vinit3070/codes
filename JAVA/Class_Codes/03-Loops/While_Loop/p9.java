//Wap to print the Multiplication of digits in given number


class Solution{
	public static void main(String[] args){
		int N=135;
		int product=1;

		while(N!=0){
			int rem=N%10;
			product=product*rem;

			N=N/10;
		}
		System.out.println("Product:"+product);
	}
}
