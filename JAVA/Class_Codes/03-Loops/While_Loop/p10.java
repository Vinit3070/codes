//Wap to print perfect squares till N
//N=30
//OP- 25-5
//     16-4
//     9-3
//     4-2
//     1-1

class Solution{
	public static void main(String[] args){
		int N=30;
		int i=1;

		while(i*i<=N){
			System.out.println(i*i);
			i++;
		}
	}
}
