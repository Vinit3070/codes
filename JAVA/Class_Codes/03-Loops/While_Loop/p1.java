//simple while loop 

class Solution{
	public static void main(String[] args){
		int i=1;
		while(i<=5){
			System.out.println(i);
			i++;
		}
	}
}
/*
 	While Loop must have Terminating condition and Increment and Decrement also.
	Without Increment and Decrement it goes into Infinite Loop
*/
