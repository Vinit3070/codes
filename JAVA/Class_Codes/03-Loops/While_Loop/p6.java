//WAP to print the numbers which are divisible by 3 between 1-50

class Solution{
	public static void main(String[] args){
		int i=1;

		while(i<=50){
			if(i%3==0){
				System.out.print(i+" ");
			}
			i++;
		}
		System.out.println();
	}
}
