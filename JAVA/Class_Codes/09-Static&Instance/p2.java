class Solution{

	static int x=10;
	static int y=20;

	static{
		System.out.println("In static block");
	}

	static void m1(){
		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String[] args){
		Solution obj=new Solution();

		System.out.println(Solution.x);					//Access using class name
		System.out.println(Solution.y);					//Access using class name
		
		System.out.println("METHOD");

		Solution.m1();							//Access using class name
		
	}
}
