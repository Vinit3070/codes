//static - static comes in program in first manner ,before main function

class Solution{

	static int x=10;
	static int y=20;

	static void fun(){
		System.out.println("Static x= "+x);
		System.out.println("Static y= "+y);
	}

	public static void main(String[] args){

		Solution obj=new Solution();

		System.out.println("X= "+x);
		System.out.println("Y= "+y);

		System.out.println("********************");

		System.out.println("Obj:x=  "+obj.x);
		System.out.println("Obj:y= "+obj.y);

		System.out.println("********************");

		System.out.println("Using Class Name::");
		System.out.println(Solution.x);
		System.out.println(Solution.y);

		System.out.println("********************");
		obj.fun();

		System.out.println("********************");

		Solution.fun();


		
	}


}
