class Solution{
	
	static{
		System.out.println("In Parent static Block");
	}

	public static void main(String[] args){
	
		System.out.println("In Solution main");
	}
}
class Child {
	static {
		System.out.println("In static block 1");
	}

	public static void main(String[] args){
		System.out.println("In Child main");
	
	}
	static {
		System.out.println("In static block 2");
	}

}
