class Solution{

	int x=10;
	static int y=20;
	static {								//1
		System.out.println("In static block-1");
		System.out.println(y);
	}
	public static void main(String[] args){
		
		System.out.println("In main Method");				//3

		Solution obj=new Solution();
		System.out.println(obj.x);
	}

	static{
		System.out.println("In static block-2");			//2
		System.out.println(y);
	
	}
}
