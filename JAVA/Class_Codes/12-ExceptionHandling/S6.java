//Interrupted Exception

class Solution{
	public static void main(String[] args){
		
		for(int i=0;i<10;i++){
			try{
				Thread.sleep(1000);		//InterruptedException : must be caught or declared to be thrown..!
			}catch(InterruptedException ie){
				
			}
			System.out.println("In main");
		}
	}
}
