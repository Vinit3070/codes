//Exception SEQUENCE for Checked Exception
//
//Exception Sequencing matters in try-catch Scenarios for CHECKED EXCEPTIONS...
//Parent class must be at last position in catch scenarios..!!
//
class Solution{
	public static void main(String[] args){
		
		for(int i=0;i<10;i++){
			try{
				Thread.sleep(1000);
			}
			/*catch(Exception obj){			
			
			}*/
			catch(InterruptedException obj){	//Error:
								//Exception has already been caught 
				
			}

			//FOR CHECKED EXCEPTIONSSo write parent classes at last position to solve this error

			System.out.println("In Main..!!");
		}
	}
}


