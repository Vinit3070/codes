
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		int arr[]=new int[5];

		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		
		System.out.println("Enter Array Elements::");


		for(int i=0;i<arr.length;i++){
			 arr[i]=Integer.parseInt(br.readLine());
		}	

		System.out.print("Array Elements are::");
		
		try{
			for(int i=0;i<10;i++){				//ArrayIndexOutofBoundException Handled using TRY-CATCH
				System.out.print(arr[i]+" ");
			}
		}catch(ArrayIndexOutOfBoundsException obj){
			
		
		}

		System.out.println();
	}
}
