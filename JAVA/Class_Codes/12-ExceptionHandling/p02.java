//Exception SEQUENCING in Unchecked Exception +Checked Exception
import java.io.*;
class Solution{
	public static void main(String[] args){
		for(int i=0;i<10;i++){
			try{
				Thread.sleep(1000);
			}
			/*catch(IOException obj){		//IOException is not thrown inside this body..!!
			
			}*/
			catch(InterruptedException obj){
				
			}
			System.out.println("In main..!!");

		}
	}
}
