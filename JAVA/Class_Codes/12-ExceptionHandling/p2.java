//Null Pointer Exception
class Solution{

	void m1(){
		System.out.println("In M1");
	}

	void m2(){
		System.out.println("In M2");
	}
	public static void main(String[] args){

		System.out.println("Start Main");
		Solution obj=new Solution();

		obj.m1();

		obj=null;			

		obj.m2();			//NullPointerException
		
		System.out.println("End Main");


	}
}
