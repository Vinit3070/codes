// Take Array of 5 numbers from user and check whether the number is not less than 0 or more than 100...
//
// If it ranges between (0-100)
//
// then , okk...>>!!!
import java.util.*;

class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String str){
		super(str);	
	}
}
class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String str){
		super(str);	
	}
}

class Solution{
	public static void main(String[] args){
		
	int arr[]=new int[5];
	Scanner sc=new Scanner(System.in);

	System.out.println("Enter Integer Value");
	System.out.println("NOTE: The Integer Value must be in between 0-100 (0<100)");

	for(int i=0;i<arr.length;i++){
		int data= sc.nextInt();

		if(data<0)
			throw new DataUnderFlowException("Value is less than 0...!!");
		if(data>100)
			throw new DataOverFlowException("Value is More than 100..!!");
		
		arr[i]=data;

	}
	for(int i=0;i<arr.length;i++){
		System.out.print(arr[i]+" ");
	}



	}
}
