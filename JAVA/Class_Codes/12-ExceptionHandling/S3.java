//Number Format Exception

import java.io.*;
class Solution{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter String::");
		String str =br.readLine();
		System.out.println(str);
		
		int num=0;
		System.out.print("Enter Integer Number::");
		try{
			num=Integer.parseInt(br.readLine());
		}catch(NumberFormatException nfe){
			System.out.println("NOTE: Please Enter Integer Data Only");
		}
		System.out.println(num);


	}
}
