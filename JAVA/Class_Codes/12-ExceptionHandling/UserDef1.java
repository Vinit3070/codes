//Throw Keyword 
//
//throw keyword is used for thorw exception manually. (not to delegate to parental one)
//User Defined Exceptions are handled using THROW keyword

import java.util.*;
class Solution{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The value of X for [10/x] ::");
		
		int x=sc.nextInt();
	
		try{
			if(x==0){
				throw new ArithmeticException("Divide by Zero");
			}
			
			System.out.println(10/x);

		}catch(ArithmeticException obj){
			//System.out.println(obj);
			//System.out.println(obj.toString());
			
			//System.out.println(obj.getMessage());
			
			System.out.print("Exception in Thread "+ "' "+ Thread.currentThread().getName()+" '"+" ");
			obj.printStackTrace();
		}
		
	}
	
}
