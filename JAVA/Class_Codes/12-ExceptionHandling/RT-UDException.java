//RealTime Example of UserDefined Exception\
//
//NCA rating
//based on nca rating the players should be included in playing 11
//
//if rating >7 then he /she must be in playing 11
//otherwise not in playing 11


import java.util.*;

class NotInPlayingXIException extends RuntimeException{
	
	NotInPlayingXIException(String msg){
		super(msg);
		
	}
}


class Solution{
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		int arr[]=new int[11];

		System.out.println("Enter Players Fitness Ratings::");
		System.out.println("NOTE: Ratings must be in between 0-10");

		for(int i=0;i<arr.length;i++){
			int FitRating=sc.nextInt();
			
			if(FitRating>0 && FitRating<10){
				if(FitRating<7)
					throw new NotInPlayingXIException ("He is Not Fully Fit..!!So Excluded From Playing XI");
				else
					System.out.println("He is Fully Fit..!!!So included in PLAYING XI");
			}

			arr[i]= FitRating;
		}
		

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}


	}
}
