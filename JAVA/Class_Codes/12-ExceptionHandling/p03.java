
//TRY_CATCH_FINALLY
//
//TRY- This block contains Risky Code
//CATCH - This block contains Handling Code
//FINALLY - This block is use to show the CONNECTION CLOSED / EXCEPTION FINALLY SUCCESSFULLY handled..!!

class Solution{

	void fun(){
		System.out.println("In fun");
	}
	public static void main(String[] args){
		
		Solution obj=new Solution();

		for(int i=0;i<5;i++){
			
			try{
				Thread.sleep(1000);				//InterruptedException
				System.out.println(10/0);			//ArithmeticException

			}catch(ArithmeticException|InterruptedException ae){	//Handling Multiple Exceptions using OR (|) Operator

				System.out.println(10/5);
				System.out.println("Exception Handled");
			}
			finally{

				System.out.println("Exception Already Handled..!!");
			}

			obj.fun();

			System.out.println("*************************************************");
		}
	}
}
