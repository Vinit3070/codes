//Interrupted Exception handling using TRY_CATCH

class Solution{
	
	public static void main(String[] args){

		for(int i=0;i<5;i++){
			
			try{
				
				Thread.sleep(500);
			}catch(InterruptedException obj){
			
			}

			System.out.println("In main");
		}
	}
}
