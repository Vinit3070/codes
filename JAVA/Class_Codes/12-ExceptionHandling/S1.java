/*
	EXCEPTION HANDLING

	EXCEPTIONS- 1) Checked(Compile time)
		    2) Unchecked(Runtime)

	CompileTime-CHECKED						RunTime-UNCHECKED
		1.IOException						1. ArithmeticException
		2.ClassNotFoundException				2. NullPointerException
	        3.InterruptedException					3. NumberFormatException
									4. IndexOutofBoundException
 ******************************************************************************************************************************************

 		Arithmetic Exception
	
*/
import java.util.*;

class Solution{

	void m1(int x,int y){
		System.out.println("In M1");
		try{
			System.out.println(x/y);		//Arithmetic Exception (/ by zero)
		}catch(ArithmeticException obj){
			
		}
	}

	public static void main(String[] args){
		Solution obj=new Solution();

		System.out.println("Start Main");

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the values of x & y::");
		int x=sc.nextInt();
		int y=sc.nextInt();

		obj.m1(x,y);

		System.out.println("End Main");
	}
}
