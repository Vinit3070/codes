class Solution{
	public static void main(String[] args){
		Solution obj=new Solution();

		obj.fun(10);
		obj.fun(10.5f);
		obj.fun('A');
		//obj.fun(10.5);		//Incompatible types : PLC from double to float
		//obj.fun(true);		//Incompatible types : Boolean cannot be converted to float

	
	}

	void fun(float x){
		
		System.out.println("In FUN");
		System.out.println("X:"+x);
	
	}

}
