//Static and Non Static Methods Access

class Solution{

	int x=10;
	static int y=20;
	public static void main(String[] args){
		
		 	Solution obj=new Solution();

			System.out.println("In main");
			System.out.println("X:"+obj.x);
			System.out.println("Y:"+obj.y);
		 
		 
			fun();
			//gun();		//Non Static methods cannot be referenced from static context
			obj.gun();
	}

	static void fun(){
		System.out.println("In Fun");

		Solution obj=new Solution();

		System.out.println("X:"+obj.x);		//Access Variable like this using obj for Non static things
							//
		System.out.println("Y:"+y);
		 
		}
	
	void gun(){
		System.out.println("In gun");
		
		System.out.println("X:"+x);
		System.out.println("Y:"+y);
		 
	}
}



