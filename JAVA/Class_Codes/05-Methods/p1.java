//Types -
//		1.Static -
//		2.Non-Static

class Solution{
	public static void main(String[] args){

		Solution obj =new Solution();
		obj.fun();


		//fun();		//Not static method cannot be referenced from static context
				//
				//Soln is - Make it Static
				//	  - Otherwise Create object and access variables /methods using objects
	}

	/* static */ 
	void fun(){
		System.out.println("In fun");
	}

}
