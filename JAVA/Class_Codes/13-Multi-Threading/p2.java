// Access Data from method which have sleep() method of Thread Class
//
//Overcoming InterruptedException using TRY_CATCH Block
//
class Solution{

	static void fun(){
		for(int i=0;i<10;i++){
			try{
				Thread.sleep(1000);
			}catch(InterruptedException obj){
			
			}

			System.out.println("In FUN");
		}
	}
	public static void main(String[] args){
		
		Solution obj=new Solution();
		
		obj.fun();
		
	}
} 
