//
//Priority Methods in Thread
//
//setPriority()- method is used for setting priority to thread
//getPriority()- method is used for getting priority of thread

class MyThread extends Thread{
	
	public void run(){
		
		System.out.println(currentThread().getName());

		System.out.println(Thread.currentThread().getPriority());

		for(int i=0;i<5;i++){
			System.out.println("In run");
		}
		
	}

}
class Solution{
	public static void main(String[] args)throws InterruptedException{
		
		MyThread obj=new MyThread();
		obj.start();

		obj.setPriority(7);					//Setting priority for the Thread-0

		//obj.sleep(1000,500);

		System.out.println(Thread.currentThread());
		
		Thread.currentThread().setPriority(9);			//Setting priority for main thread

		System.out.println(Thread.currentThread().getPriority());
	}
}
