//JOIN()
class MyThread extends Thread{
	public void run(){
		
		for(int i=1;i<=10;i++){
		
			for(int j=1;j<=10;j++){
		
				System.out.println(i+" * "+j+" = "+(i*j));
		} 

		System.out.println();
		}		

	}
}

class Solution{
	public static void main(String[] args)throws InterruptedException{
		
		MyThread obj=new MyThread();
		obj.start();
	
		obj.join();

		for(int i=0;i<10;i++){
			System.out.println("In main");
		}

		System.out.println("END Main");
	}
}
