//In MultiThreading There are two ways for Making Thread ..
//
//1.Creating Thread using Thread Class
//2.Creating Thread using RunnaableInterFace
//
//1.Creating Thread Using Thread Class

class MyThread extends Thread{
	
	public void run(){
		
		for(int i=0;i<5;i++){

			try{
				Thread.sleep(100);		//Throws InterruptedException

			}catch(InterruptedException ie){
			
			}
			
			System.out.println("In RUN");
		}
	}
}
class Solution{
	
	public static void main(String[] args)throws InterruptedException{
			
		MyThread obj=new MyThread();		//The First Thread is Created (i.e Thread-0)
		obj.start();				//start() method is of Thread class 
							//In the start() method there is a code like call for run()
		
		for(int i=0;i<5;i++){
			
			Thread.sleep(100);
			System.out.println("In main");
		}


	}
}
