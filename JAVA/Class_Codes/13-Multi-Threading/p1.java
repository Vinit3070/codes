
// MultiThreading In Java
// It is used for Speed Optimization...
// It is achieved using Trasferring Tasks/Work Between Multiple Threads...
// If Multiple Threads does Assigned Tasks effectively then the process Execution Becomes Faster..!!
//
//
//Accessing Methods have sleep() of thread class ....
//but ,InterruptedException is Thrown ...so we overcome from it with 2 ways- 
//										1. Throws keyword
//										2. Try-catch Block 
//
//1.Throws Keyword for Throwing Exception to Parental One..										
class Solution{

	static void fun()throws InterruptedException{
		for(int i=0;i<10;i++){
			System.out.println("In FUN");

			Thread.sleep(2000);
		}
	}
	public static void main(String[] args)throws InterruptedException{
		
		Solution obj=new Solution();

		obj.fun();
	}
}
