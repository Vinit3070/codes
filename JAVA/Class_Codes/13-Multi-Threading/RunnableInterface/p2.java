
class MyThread implements Runnable{
	
	public void run(){
		for(int i=1;i<=10;i++){
			System.out.println("In run");
		}
	}

	
}
class Solution{

	public static void main(String[] args)throws InterruptedException{
	
		MyThread obj=new MyThread();
		
		Thread t=new Thread(obj);
		t.start();

		t.sleep(2000,500);
		
		for(int i=1;i<=10;i++){
			System.out.println("In main");
		}
	}
}
