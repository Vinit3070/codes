
class MyThread implements Runnable{
	
	public void run(){

		for(int i=1;i<=10;i++){
		
			System.out.println("In Thread -0");
		}	
	}
}
class Solution{
	
	public static void main(String[] args)throws InterruptedException{

		MyThread obj=new MyThread();
		Thread t=new Thread(obj);
		
		t.start();
		
		System.out.println(t.currentThread());

		t.join(10000);						//Join()
		
		Thread.currentThread().setName("Core2Web");
		
		for(int i=1;i<=10;i++){
			System.out.println(t.currentThread());
		}
		
		
	}
}
