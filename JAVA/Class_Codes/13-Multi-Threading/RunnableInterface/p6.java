//yield()
class MyThread implements Runnable{
	
 	public void run(){
		for(int i=0;i<10;i++){
			System.out.println("IN RUN");
		}
	}
}
class Solution{
	
	public static void main(String[] args){	

		MyThread obj=new MyThread();
		Thread t= new Thread(obj);
		t.start();
		
		t.yield();				//this method is use to give our work to other with reference of priority

		for(int i=0;i<10;i++){
			System.out.println("IN MAIN");
		}
	}
}
