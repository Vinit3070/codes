
class MyThread implements Runnable{
	
	public void run(){
		
		System.out.println("In Thread -0");
		
	}
}
class Solution{
	
	public static void main(String[] args)throws InterruptedException{

		MyThread obj=new MyThread();
		Thread t=new Thread(obj);
		
		t.start();
		
		System.out.println(t.currentThread());

		t.sleep(2000);
		
		Thread.currentThread().setName("Core2Web");
	
		System.out.println(t.currentThread());
		
		
	}
}
