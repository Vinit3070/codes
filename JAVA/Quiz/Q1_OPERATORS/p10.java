
class Solution{
	public static void main(String[] args){
		int a=10,b=10,c=10,d=10;

		System.out.println(++a);	//Increment and then print = 11
		System.out.println(b++);	//Print and then Increment = 10

		System.out.println(+-c);	//Assign - sign to c = -10
		System.out.println(-++d);	//Assign - sign with an increment = -11
						
		
		System.out.println(-++a);	//Assign - sign with increment = -12
		System.out.println(+--b);	//Assign + sign with decrement = 10
	}
}
