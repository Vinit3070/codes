
class Solution{
	public static void main(String[] args){
		if(10){
			System.out.println("Inside if1");
		}else{
			System.out.println("Inside else");
		}
	}
}

//OP- Comp time Error 
//	In Java for Control statements conditions result must be boolean..
//	in this case --> Incompatible types
//			Int cannot be converted into boolean
