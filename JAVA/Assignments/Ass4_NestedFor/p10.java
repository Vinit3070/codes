/*
	Pattern 10-

		1  2  3  4
		2  3  4  5 
		3  4  5  6
		4  5  6  7

*/
class Solution{
	public static void main(String[] args){
		int num=1;
		int num2=1;
		
		for(int i=1;i<=4;i++){
			
			for(int j=1;j<=4;j++){
				System.out.print((num++)+" ");	
			}
			num2++;
			num=num2;
			
			System.out.println();	
		}
	
	}
}
