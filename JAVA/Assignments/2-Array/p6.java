/*	
	Wap to print the maximum element from the array
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an Array::");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int max=arr[0];

		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
			
				max=arr[i];
			}
		}
		System.out.print("The Maximum Element in an Array are:: " +max );
		System.out.println();

		
	}
}
