/*
 
	Wap to print the Elements whose addition of digits is even
	Arr= 1 2 15 24 32 34 76 42 89
	output= 3 15 24 32 42 
*/


import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
				
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of array::");
		int size= Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("Array Elements are::");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		
		System.out.println();

		System.out.print("Array Elements whose Sum is even are:: ");
		int sum=0;
		for(int i=0;i<arr.length;i++){
			sum=0;
			int Samp_arr=arr[i];
			
			while(Samp_arr != 0){
				int rem= Samp_arr % 10;
				sum=sum+rem;
				Samp_arr=Samp_arr/10;	
			}

			if(sum%2==0){
				System.out.print(arr[i]+" " );

			}
		}
		System.out.println();
	}
}
