/*	
	Wap to create an array of N elements and find the number of even and odd integers in given array
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an Array::");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println();

		int oddCnt=0;
		int evenCnt=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				evenCnt++;	
			}
			else{
				oddCnt++; 
			}

		}

		System.out.println("Number of Even Elements in an Array are:: " +evenCnt);
		System.out.println("Number of Odd Elements in an Array are:: " +oddCnt);
	}
}
