/*	
	Wap to create an array of N elements and print the sum of even and odd integers in given array
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an Array::");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println();

		int evenSum=0;
		int oddSum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				evenSum=evenSum+arr[i];	
			}
			else{
				 
				oddSum=oddSum+arr[i];	
			}

		}

		System.out.println("Sum of Even Elements in an Array are:: " +evenSum);
		System.out.println("Sum of Odd Elements in an Array are:: " +oddSum);
	}
}
