/*	
	Wap to create an array of N elements also take both size and elements from the user and print the sum of elements
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an Array::");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println();

		int sum=0;

		for(int i=0;i<arr.length;i++){
			sum=sum+arr[i];

		}

		System.out.println("Sum of Array elements is :: "+sum);
	}
}
