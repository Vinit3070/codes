/*	
	Wap to print the common elements in both arrays
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of First Array::");
		int size1= Integer.parseInt(br.readLine());
		
		int arr1[]=new int[size1];
		System.out.print("Enter Array Elements of First Array::");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Enter the Size of Second Array::");
		int size2= Integer.parseInt(br.readLine());
		
		int arr2[]=new int[size2];
		System.out.print("Enter Array Elements of Second Array::");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("The Common elements in both arrays are:: ");
		
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
			
					System.out.print(arr1[i]+" ");
				}
			}
		}
		
		System.out.println();

		
	}
}
