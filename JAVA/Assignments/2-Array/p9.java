/*
	Wap to Merge two given Arrays...

	1- 10 20 30 40 50
	2- 60 70 80 90 100

	After Merge- (3)--> 10 20 30 40 50 60 70 80 90 100
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
				
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
//1st
		System.out.print("Enter the Size of first array::");
		int size1= Integer.parseInt(br.readLine());
		int arr1[]=new int[size1];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("First Array's Elements are::");
		for(int i=0;i<arr1.length;i++){
			System.out.print(arr1[i]+" ");
		}
		System.out.println();
//2nd 
		System.out.print("Enter the Size of Second array::");
		int size2= Integer.parseInt(br.readLine());
		int arr2[]=new int[size2];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("Second Array's Elements are::");
		for(int i=0;i<arr2.length;i++){
			System.out.print(arr2[i]+" ");
		}
		System.out.println();

//3rd		

		int arr3[]=new int[size1+size2];
	
		for(int i=0;i<arr1.length;i++){
			arr3[i]=arr1[i];	
		}
		for(int i=0;i<arr2.length;i++){
			arr3[size2+i]=arr2[i];
		}
//After Merging-->
		System.out.println("After Merging Third Array Elements are:: " );

		for(int i=0;i<arr3.length;i++){

			System.out.print(arr3[i]+" ");
		}
			
		System.out.println();


	}
}
