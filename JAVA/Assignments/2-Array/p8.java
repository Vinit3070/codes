/*	
	Wap to print the uncommon elements in both arrays
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of First Array::");
		int size1= Integer.parseInt(br.readLine());
		
		int arr1[]=new int[size1];
		System.out.print("Enter Array Elements of First Array::");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Enter the Size of Second Array::");
		int size2= Integer.parseInt(br.readLine());
		
		int arr2[]=new int[size2];
		System.out.print("Enter Array Elements of Second Array::");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("The Uncommon elements in first array are:: ");
		int flag =0;
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					flag=1;
				}
			}
			if(flag==0)
			System.out.print(arr1[i]+" ");

			flag = 0;
		}

	//	System.out.println();
		
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr2[i]==arr1[j]){
					flag=1;
				}
			
			}
			if(flag == 0)
			System.out.print(arr2[i]+" ");

			flag = 0;
		 
		}
		
		
		System.out.println();

		
	}
}
