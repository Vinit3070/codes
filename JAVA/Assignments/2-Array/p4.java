/*	
	Wap to Search Specific element in given array and print its index
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of an Array::");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Enter the element do you want to search::");
		int key= Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			if(arr[i]==key){
				System.out.print("Index of Our expected Key element:: "+i);
			}
		}
		System.out.println();

		
	}
}
