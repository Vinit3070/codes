//WAP to print reverse from 100-1

class Solution{
	public static void main(String[] args){
		for(int i=100;i>=1;i--){
			System.out.print(i+" ");
		}
		System.out.println();
	}
}
