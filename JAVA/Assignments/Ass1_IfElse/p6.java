//WAP that checks a number from 0-5 and prints its spelling .If the number is greater than 5 print the number is greater than 5.

class Solution{
	public static void main(String[] args){
		
		int no=9;

		if(no>=0 && no<=5){

			if(no==0)
				System.out.println("ZERO");
			if(no==1)
				System.out.println("ONE");
			if(no==2)
				System.out.println("TWO");
			if(no==3)
				System.out.println("THREE");
			if(no==4)
				System.out.println("FOUR");
			if(no==5)
				System.out.println("FIVE");
		}
		else if(no>5){
			System.out.println("The Number is Greater than 5");
		}else{
			System.out.println("INVALID INPUT!!!! The Number is less than 0..");
		}
	}
}
