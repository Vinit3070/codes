//WAP To check day number (1-7) and print the corresponding day of the week.

class Solution{
	public static void main(String[] args){
		int Day=7;

		if(Day>=1 && Day<=7){
			if(Day==1)
				System.out.println("MONDAY");
			
			if(Day==2)
				System.out.println("TUESDAY");
			
			if(Day==3)
				System.out.println("WEDNESDAY");
			
			if(Day==4)
				System.out.println("THURSDAY");
			
			if(Day==5)
				System.out.println("FRIDAY");
			
			if(Day==6)
				System.out.println("SATURDAY");
			
			if(Day==7)
				System.out.println("SUNDAY");
			
		}else{
			System.out.println("INVALID INPUT DAY!!!");
		}
	}
}
