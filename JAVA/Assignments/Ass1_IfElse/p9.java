//WAP that take 3 Numbers and state whether it is pythagorean triplet or not.

class Solution{
	public static void main(String[] args){
		int a=3;
		int b=4;
		int c=2;

		if(a>b && a>c){
			
			if((b*b+c*c)==a*a){
				System.out.println("It is a Pythagorean Triplet");
			}else{
				System.out.println("It is not a Pythagorean Triplet");
			}
		}
		else if(b>a && b>c){
			
			if((c*c+a*a)==b*b){
				System.out.println("It is a Pythagorean Triplet");
			}else{
				System.out.println("It is not a Pythagorean Triplet");
			}
		}
		else if(c>b && c>a){
			
			if((b*b+a*a)==c*c){
				System.out.println("It is a Pythagorean Triplet");
			}else{
				System.out.println("It is not a Pythagorean Triplet");
			}
		}
		else {
			System.out.println("It is Not a Pythagorean Triplet");	
		}
	
	
	}
}
