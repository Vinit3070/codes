/*
	Pattern 9-
			1C3	4B2	9A1
			16C3	25B2	36A1
			49C3	64B2	81A1
*/

class Solution{
	public static void main(String[] args){
		
		int num=1;
		
		for(int i=1;i<=3;i++){
			
			char ch='C';
			int N=3;
			for(int j=3;j>=1;j--){
				System.out.print(num*num + "" + ch-- + "" + j + "\t");
				/*System.out.print((num*num));
				System.out.print(ch);
				System.out.print(N--);
				System.out.print("	");
				num++;
				ch--;*/
				num++;
			}
			System.out.println();
		}
	}
}
