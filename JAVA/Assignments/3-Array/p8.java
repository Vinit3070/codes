/*
	Wap to find an Armstrong Number from an array and return its index 
	153=
	(1*1*1 + 5*5*5 +3*3*3)=(1+125+27 =153 )
*/	
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of an Array::");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] =new int[size];

		System.out.print("Enter the Array Elements::");
		for(int i=0;i<arr.length ;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}									
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

		System.out.println();

		for(int i=0;i<arr.length;i++){
			int count=0;
			int sum=0;
			int num=arr[i];

			for(int a=num;a!=0;a=a/10){
				count++;
				
			}
			num=arr[i];

			for(int j=num;j!=0;j=j/10){
				int mul=1;
				int rem=j%10;

				for(int k=1;k<=count;k++){
					
					mul=mul*rem;
						
				}

				sum=sum+mul;

			}

			if(sum==arr[i]){
				System.out.println("Armstrong Number is found at index :"+i);
			}
			
		}

		System.out.println();

	}

}
		
