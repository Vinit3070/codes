/*
 	Wap to find palindrome no and print its index
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of an Array::");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] =new int[size];

		System.out.print("Enter the Array Elements::");
		for(int i=0;i<arr.length ;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

		System.out.println();

		for(int i=0;i<arr.length;i++){
			int temp=arr[i];
			int x =temp;

			int rev=0;
			for(int j=arr[i];j!=0;j=j/10){
				int rem=temp%10;
				rev=rev*10+rem;
				temp=temp/10;
			}
			if(x==rev)
				System.out.println("The Palindrome Number is at index:"+i);	
		}

	}
}

		
