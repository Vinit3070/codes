/*
	Wap to find a perfect number in given array and print its index
	6= 1+2+3(divisors)
	28=1+2+4+7+14
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of an Array::");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] =new int[size];

		System.out.print("Enter the Array Elements::");
		for(int i=0;i<arr.length ;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		
		System.out.println();
		for(int i=0;i<arr.length;i++){
			int sum=0;
			int temp=arr[i];
			for(int j=1;j<arr[i];j++){
				if(arr[i]%j==0){
					sum=sum+j;
				}
			}
			if(sum==temp){
				System.out.println("The Perfect Number is at Index"+i);
			}
		}
	}
}
		
