/*	
	Wap to find Strong number in an array and print its index
	145= (1! +4! +5!)= 
	(1+24+125)=145
*/


import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of an Array::");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] =new int[size];

		System.out.print("Enter the Array Elements::");
		for(int i=0;i<arr.length ;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

		System.out.println();

		for(int i=0;i<arr.length;i++){
			int sum=0;
			int rev=0;
			int temp=arr[i];
			
			while(temp!=0){
				int fact=1;
				int rem=temp%10;

				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}

				temp=temp/10;
				sum=sum+fact;
			}
			if (sum==arr[i])
			System.out.println("The strong Number is at index::"+i);
		}
	}
}
		
