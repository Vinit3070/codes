/*
 	Wap to reverse each element from the array
	take size and elements fram user
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.print("Enter the Size of the Array::");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		for(int i=0;i<arr.length;i++){
			int temp=arr[i];
			int rev=0;

			while(temp!=0){
				int rem=temp%10;
				rev=rev*10+rem;
				temp=temp/10;
				if(rem==0)	
				System.out.print(0+rev);
			}
			
			System.out.print(rev+" ");
		}
	}
}
