/*
	Wap to print the count of digits in elements of array
*/

import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of the array::");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.print("Enter Array Elements::");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.print("Count of digits in each Array Element is:: ");
		for(int i=0;i<arr.length;i++){
			int temp=arr[i];
			int count=0;

			while(temp!=0){
				count++;
				temp=temp/10;
			}
			System.out.print(count+" ");
		}
		System.out.println();
	}
}
