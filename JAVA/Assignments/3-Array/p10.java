/*

	Wap to print the second min element in the array

*/


import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of an Array::");
		int size=Integer.parseInt(br.readLine());
		
		int arr[] =new int[size];

		System.out.print("Enter the Array Elements::");
		for(int i=0;i<arr.length ;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

		for(int i=0;i<arr.length;i++){
			int max=arr[i];

			for(int j=i+1; j<arr.length;j++){

				if(arr[i]>arr[j]){
					
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		
				System.out.println("The Second max Element in an array is::"+arr[1]);		
															//arr[1]is the index which is second minimum element in an array
	}
}

