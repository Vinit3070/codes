/*
	Wap that takes 10 integer numbers and only print the numbers which are divisible by 5
*/

import java.io.*;

class Solution{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter the size of the Array: ");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		System.out.print("Enter the Array Elements::");

		for(int i=0;i<arr.length;i++){
			arr[i]=	Integer.parseInt(br.readLine());
		}

		System.out.println();
		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0){
				System.out.print(arr[i]+" ");
			}
		}
			
		System.out.println();

	}
}
