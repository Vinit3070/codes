/*
	Wap to take size of array from the user and take integer elements and print the sum of odd elements only
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of the Array::");
		int size= Integer.parseInt(br.readLine());

		int sum=0;

		System.out.print("Enter the Array Elements::");

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}
		
		System.out.println();

		for(int i=0;i<arr.length;i++){
			if(arr[i]%2!=0){
				sum=sum+arr[i];
			}
		}
		System.out.print("Sum of Odd elements in Array:: " +sum);
		System.out.println();
	}

}
