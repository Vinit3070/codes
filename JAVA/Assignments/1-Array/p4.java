/*

	Wap that takes 7 characters as an input and print only VOWELS from the Array..

*/
import java.util.*;
class Solution{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of the Array::");
		int size=sc.nextInt();

		char carr[]=new char[size];
		System.out.print("Enter the Characters::");
		for(int i=0;i<carr.length;i++){
			carr[i]=sc.next().charAt(0);
		}
		for(int i=0;i<carr.length;i++){
			if ( carr[i]=='a'|| carr[i]=='A'||
		             carr[i]=='e'|| carr[i]=='E'||
			     carr[i]=='i'|| carr[i]=='I'||
			     carr[i]=='o'|| carr[i]=='O'|| 
			     carr[i]=='u'|| carr[i]=='U')
			{
				System.out.print(carr[i]+" ");     
			}
		}
		System.out.println();
	}
}
