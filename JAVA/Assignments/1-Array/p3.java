/*
	Wap to take size of array from the user and take integer elements and print the product of odd index only
*/
import java.io.*;
class Solution{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the Size of the Array::");
		int size= Integer.parseInt(br.readLine());

		int product=1;

		System.out.print("Enter the Array Elements::");

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}
		
		System.out.println();

		for(int i=0;i<arr.length;i++){
			if(i%2!=0){
				product= product * arr[i];
			}
		}
		System.out.print("Product of odd indexed elements in Array:: " +product);
		System.out.println();
	}

}
