// Wap to state whther the given number is strong number or not..
// Strong no= 145
// sum of fact of each digit in given no= given number
// 1! + 4! + 5! =145 
// 1+24+120= 145
//
class Solution{
	public static void main(String[] args){
		int num=14;
		int temp=num;
		int sum=0;

		for (int i=num;i!=0;i=i/10){
			int fact=1;

			int rem=i%10;

			for(int j=1;j<=rem;j++){
				fact=fact*j;
			}

			sum= sum+fact;
		}

		if(sum==temp){
			System.out.println("Strong Number");
		}else{
			System.out.println("Not Strong Number");
		}

	}
}
