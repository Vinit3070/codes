/*
 Pattern3-
 		10  
		9   8
		7   6   5
		4   3   2   1
*/
class Solution{
	public static void main(String[] args){
		int num=10;
		int rows=4;

		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t ");
				num--;
			}
			System.out.println();
		}
	}
}

