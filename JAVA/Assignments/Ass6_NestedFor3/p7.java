/*
 Pattern7-  	  
	
	F
	E  F
	D  E  F
	C  D  E  F
	B  C  D  E  F
	A  B  C  D  E  F
*/
class Solution{
	public static void main(String args[]){
		int rows=6;

		char ch='F';
		char ch2;

		for(int i=1;i<=rows;i++){
			ch2=ch;
			for(int j=1;j<=i;j++){
				System.out.print(ch2+"\t");
				ch2++;
					
			}
			System.out.println();
			ch--;
		}
	}
}
