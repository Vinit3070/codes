/*Pattern2-
		1
		2  3
		4  5  6
		7  8  9  10
*/

class Solution{
	public static void main(String[] args){
		
		int num=1;
		int rows=4;

		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t");
				num++;
			}
			System.out.println();	
		}
	}
}
