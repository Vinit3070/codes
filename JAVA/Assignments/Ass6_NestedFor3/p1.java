/*Pattern1-	
		C2W	
		C2W	C2W	
		C2W	C2W	C2W
		C2W	C2W	C2W	C2W

*/

class Solution{
	public static void main(String[] args){
		int rows=4;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				System.out.print("\t"+"C2W");
			}
			System.out.println();
		}
	}
}
