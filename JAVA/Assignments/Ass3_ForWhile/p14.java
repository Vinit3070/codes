//WAP to count odd digits of the given number
class Solution{
	public static void main(String[] args){
		int N=521357782;
		int count=0;

		for(int i=N;N!=0;N=N/10){
			if(N%2!=0){
				count++;
			}
		}
		System.out.println("COUNT:"+count);
	}
}
