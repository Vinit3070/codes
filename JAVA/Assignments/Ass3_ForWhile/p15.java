//Wap to print the square of even digits of the given number

class Solution{
	public static void main(String[] args){
		int N=942111423;
		for(int i=N;N!=0;N=N/10){
			int rem=N%10;
			if(N%2==0){
				System.out.println(rem*rem);
			}
		}
		
		/*

		while(N!=0){
			int rem=N%10;
			if(rem%2==0){
				System.out.println(rem*rem);
			}
			N=N/10;
		}*/
	}
}
