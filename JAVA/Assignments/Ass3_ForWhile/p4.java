//Wap to print the odd digits of given number

class Solution{
	public static void main(String[] args){
		int N=352647;
		int temp=N;
		int count=0;
		while(N!=0){
			int rem=N%10;
			if(rem%2!=0){
				count++;
			
			}
			N=N/10;
		}
		System.out.println("COUNT OF ODD DIGITS IN "+temp+" IS= "+count);
	}
}
