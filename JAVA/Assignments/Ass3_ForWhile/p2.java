//Wap to calculate the factorial of given number using while loop

class Solution{
	public static void main(String[] args){
		int fact=1;
		int i=1;
		int Num=5;

		while(i<=Num){
			fact=fact*i;
			i++;
		}
		System.out.println("Fact of "+Num+" is :"+fact);
	}
}
