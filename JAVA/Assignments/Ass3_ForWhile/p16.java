//Wap to print the sum of all even numbers and multiplication of all odd numbers between 1-10

class Solution{
	public static void main(String[] args){

		int sum=0;
		int product=1;

		for(int i=1;i<=10;i++){
			if(i%2==0){
				sum=sum+i;			//2+4+6+8+10=30
			}else{
				product=product*i;		//1*3*5*7*9=945
			}
		}
		System.out.println("SUM:"+sum);
		System.out.println("PRODUCT:"+product);
	}
}
