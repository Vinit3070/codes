//Wap to count the digits of given number using while loop

class Solution{
	public static void main(String[] args){
		int N=773253;
		
		int count=0;

		while(N!=0){
			int rem=N%10;
			count++;
			N=N/10;
		}
		System.out.println("COUNT-"+count);
	}
}
