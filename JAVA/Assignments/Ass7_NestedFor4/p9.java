/*Pattern 9-		
		1	
		8	9
		27	16	125
		64	25	216	49
*/

class Solution {
	public static void main(String[] args){
		
		int rows=4;

		for(int i=1;i<=rows;i++){
			int n=i;
		
			for(int j=1;j<=i;j++){
				if(j%2!=0){
					System.out.print((n*n*n)+"\t");
					
				}
				else{
					System.out.print((n*n)+"\t");
				}
				n++;
			}
			System.out.println();
		}
	}
}
