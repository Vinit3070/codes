/*
Pattern 8-
		10
		I	H
		7	6	5	
		D	C	B	A
*/

class Solution{
	public static void main(String[] args){
		int rows=4;

		int num=(rows*rows+rows)/2;
		char ch='J';

		for (int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print(ch+"\t");
					
				}else{
					System.out.print(num+"\t");
					
				}
				num--;
				ch--;
			}
			System.out.println();
		}

	}
}
