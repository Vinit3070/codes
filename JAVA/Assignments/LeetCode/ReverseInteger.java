/*

1. Reverse Integer (Leetcode:- 7)

Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231
- 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed
or unsigned).
Example 1:
Input: x = 123
Output: 321
Example 2:
Input: x = -123
Output: -321
Example 3:
Input: x = 120
Output: 21

Constraints:
-231 <= x <= 231 - 1

*/


import java.util.*;
class Solution{
		
	static int ReverseNo(int no){
		
		int reversedNo = 0;

       		while (no != 0) {
           		int digit = no % 10;
            		reversedNo = reversedNo * 10 + digit;
            		no /= 10;
			
		}
		return reversedNo;

	}

	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		
		System.out.print("Enter The No:: ");
		int no=sc.nextInt();
		System.out.println("Reversed No:: "+Solution.ReverseNo(no));		
	}
}
