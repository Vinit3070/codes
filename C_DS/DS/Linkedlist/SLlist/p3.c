#include<stdio.h>
#include<stdlib.h>
typedef struct node
{
	int data;
	struct node *next;
}Node;

Node* head=NULL;

Node * createNode(){
	Node *newNode =(Node*)malloc(sizeof(Node));

	int data;
	printf("\nENTER THE DATA::");
	scanf("%d",&newNode->data);

	newNode->next=NULL;
	return newNode;

}
void addNode(){
	
	Node *newNode=createNode();

	if(head==NULL)
	{
		head=newNode;
		newNode->next=head;
	}
	else{
		Node *temp=head;

		while(temp->next!=head)
		{
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
	
	}

}
void addFirst(){
	Node* newNode=createNode();

	if(head==NULL)
	{
		head=newNode;
		newNode->next=head;

	}
	else{
		Node *temp=head;
		while (temp->next!=head)
		{
			temp=temp->next;
		}

		newNode->next=head;
		head=newNode;
		temp->next=newNode;
	}

}
int countNode(){
	int count=0;
	Node *temp=head;
	while(temp->next!=head)
	{
		count++;
		temp=temp->next;
	}
	return count ;

}


int addAtPos(int pos){

	int count=countNode();

	if(pos<=0 || pos> count +2)
	{
		printf("\nInvalid Position...");
		return -1;
	}
	else
	{
		if (pos==1)
		{
			addFirst();
		}
		else if(pos==count+1)
		{
			addNode();
		}
		else
		{
			Node *temp=head;
			Node *newNode=createNode();
			while (pos-2)
			{
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		
		}
	}
	return 0;
}

void printSCLL(){
	Node*temp =head;
	while (temp->next!=head)
	{
		printf("|%d|-->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
	

}

void delFirst(){
	if(head==NULL){
		printf("\nEMPTY LINKEDLISt");
	
	}
	else if( head->next==head)
	{
		free(head);
		head=NULL;
	}
	else{

		Node *temp=head;
		while (temp->next!=head){
			temp=temp->next;
		}
		temp->next=head->next;
		free(head);
		head=temp->next;

	}

}
void delLast(){
	
	if(head==NULL)
	{
		printf("\nEMPTY LINKED LIST");

	}
	else if(head ->next==head){
		free(head);
		head=NULL;
	}
	else{
		Node *temp=head;
		while(temp->next->next!=head){
			temp=temp->next;
		}
			free(temp->next);
			temp->next=head;
	}

}
int delAtPos(int pos){
	int count =countNode();

	if(pos<=0 || pos >count+1)
	{
		printf("\nINVAALID POSITION");
		return -1;
	}
	else{
		if(pos==1)
		{
			delFirst();
		}
		else if(pos==count)
		{
			delLast();
		}
		else{
			Node *temp=head;
			Node *temp1=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}

			temp1= temp->next;
			temp->next=temp1->next->next;
			free(temp1);
		}
		return 0;
	
	}
	

}

void main(){
	
	while(1){
		printf("\n\t***MENU***\t\n");
		printf("\n1:ADDNODE: ");
		printf("\n2:ADDFIRST: ");
		printf("\n3:ADDATPOS: ");
		printf("\n4:DELFIRST: ");
		printf("\n5:DELLAST: ");
		printf("\n6:DELATPOS: ");
		printf("\n7:PRINTSCLL ");
		printf("\n8:EXIT:\n");
		
		int ch;
		printf("\nENTER YOUR CHOICE:");
		scanf("%d",&ch);

		switch(ch)
		{
			case 1:
				addNode();
				break;

			case 2:
				addFirst();
				break;
			case 3:
				{
				int pos;
				printf("\nENTER THE POSITION FOR INSERTING NODE::");
				scanf("%d",&pos);
				addAtPos(pos);
				}
			case 4:
				delFirst();
				break;

			case 5:
				delLast();
				break;

			case 6: {
				int pos;
				printf("\nENTER THE POSITION FOR DELETING NODE::");
				scanf("%d",&pos);
				delAtPos(pos);
				}
			case 7:
				printSCLL();
				break;

			case 8:
				exit(0);

			default:
				printf("\nWRONG INPUT...");
				break;
		
		}
	}


}
