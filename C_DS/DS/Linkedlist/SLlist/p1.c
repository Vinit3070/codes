
#include<stdio.h>
#include<stdlib.h>
typedef struct Node  {
	int data; 
	struct Node *next;

}node;

node * head=NULL;

node *createNode(){
	node* newNode = (node*) malloc(sizeof(node));
	
	printf("\n Enter the DATA:");
	scanf("%d",&newNode->data);

	newNode->next =NULL;
	return newNode;

}
void addNode(){
	
	node *newNode= createNode();

	if (head == NULL)
	{
		head = newNode;
	}
	else
	{
		node* temp=head;
		while(temp->next!=NULL)
		{
			temp=temp->next;
		}
		temp->next=newNode;
		
	}
}

void addFirst(){

	node * newNode=createNode();

	if(head ==NULL)
	{
		head=newNode;
	}
	else
	{
		newNode->next=head;
		head=newNode;
	}
}

void printList(){

	node *temp= head;

	if (head==NULL)
	{
		printf("\n Linked list is Empty");
	}	
	else{
		while(temp!=NULL)
		{
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("NULL\n");
	}
}

int countNode(){
	
	int count=0;
	node* temp= head;
	while(temp!=NULL)
	{
		
		count++;
		temp=temp->next;
	}

	return count;
}
int addAtPos(){

	int pos;
	int count =countNode();
	printf("\n Enter Position to insert node :");
	scanf("%d",&pos);
	
	if (pos <=0  || pos>=count+2 ) 
	{
		printf("\n Invalid Position ");
		return -1;
	}
	else if(pos==count+1)
	{
		addNode();
	}
	else if(pos==1)
	{
		addFirst();
	}
	else
	{
		node *newNode=createNode();	
		node *temp=head;
		while (pos-2)
		{
			temp=temp->next ;
			pos--;

		}
		newNode->next=temp->next;
		temp->next=newNode;
	}
}


void main(){

	int no;

	printf("\n Enter the Number of Nodes:");
	scanf("%d",&no);

	for(int i=1;i<=no;i++)
	{
		addNode();
	}

	//addFirst();
	printList();

	printf("\nCOUNT::%d\n",countNode());

	addAtPos();
	printList();


 
}
