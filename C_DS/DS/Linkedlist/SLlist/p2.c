
#include<stdio.h>
#include<stdlib.h>


typedef struct Node{

	struct Node *prev;
	int data;
	struct Node * next;
	
}node;

node* head=NULL;

node * createNode(){

	node *newNode= (node*)malloc(sizeof(node));

	newNode->prev=NULL;
	printf("\nENTER DATA::");
	scanf("%d",&newNode->data);
	newNode->next=NULL;

	return newNode;
	
}
int addNode(){
	
	node * newNode= createNode();

	if(head==NULL)
	{
		head=newNode;
	}
	else
	{
		node *temp= head;	
		while(temp->next!=NULL)
		{
			temp=temp->next;
		}

		temp->next= newNode;
		newNode->prev= temp;
	}

}

void printDLL(){
	
	node *temp= head;
	while(temp!=NULL)
	{
		printf("|%d|->",temp->data);
		temp=temp->next;
	}

	printf("NULL\n");

}
void addFirst(){

	node *newNode=createNode();
	 
	if(head==NULL)
	{
		head=newNode;
	}
	else
	{
		head->prev= newNode;
		newNode->next=head;
		head=newNode;
	
	}
	
}

int countNode(){
	int count =0;
	node *temp=head;
	while(temp!=NULL)
	{
		count++;
		temp=temp->next;
	}
	
	//printf("COUNT:%d",count);
	return count;

}

int addAtPos(int pos){
 	int count=countNode();
	

	if (pos<=0 || pos> count+1)
	{
		printf("\nInvalid Position..");
		return -1;
	}
	else
	{
		if (pos==1)
		{
			addFirst();
		}
		else if(pos==count+1)
		{
			addNode();
		}
		else
		{
			node *temp=head ;
			node *newNode =createNode();

			while(pos-2)
			{
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
			temp->next->prev=newNode;
			temp->next=newNode;
		}
	}
}

int delFirst()
{
	if(head==NULL)
	{
		printf("\nEMPTY LINKEDLIST");
		return -1;
	}
	else
	{
		node* temp=head;
		head=temp->next;
		head->prev=NULL;
		free(temp);
		return 0;
	
	}

}

int delLast(){
	 
	if (head==NULL)
	{
		printf("\nNO NODE TO DELETE");
		return -1;
	}
	else
	{
		node *temp=head;
		while(temp->next->next!=NULL)
		{
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
	printf("\nLASTNODE DELETED");
	return 0;

}

int delAtPos(int pos)
{
	int count=countNode();
	

	if(pos<=0 || pos >count+2)
	{

		printf("\n Invalid input");
		return -1;
	}
	

	else if(pos==1) 
		{
			delFirst();
		}
	else if(pos==count)
		{
			delLast();
		}
	else
		{
			node *temp =head;
			while(pos-2)
			{
				temp=temp->next;
				pos--;
			}
		
			node *temp1= temp->next->next;
			free(temp->next);
			temp->next=temp1;
		}
		
	}



void main(){

	char choice;
	while(1){

		printf("\n1: AddNode:");
		printf("\n2: AddFirst:");
		printf("\n3: AddAtPos:");
		printf("\n4: DelFirst:");
		printf("\n5: DelLast:");
		printf("\n6: DelAtPos:");
		printf("\n7: PrintDLL:");
		printf("\n8: Exit");
		printf("\n9: NODECount:\n\n");

		
		int ch;
		printf("\nEnter Your Choice::");
		scanf("%d",&ch);

		switch(ch)
		{
			case 1: 
				addNode();
		
				break;
			 
			case 2: 
				addFirst();
				
				break;
		
		
			case 3: {
				int pos;
				printf("\nEnter the position to insert node: ");
				scanf("%d",&pos);
				addAtPos(pos);
				
				break;
			}
		
			case 4: 
				delFirst();
			
				break;
		
			case 5: 
				delLast();
				
				break;
		
			case 6: {
				int pos;
				printf("\nEnter the position to delete node: ");
				scanf("%d",&pos);
				delAtPos(pos);
				
				break;
			}

			case 7:
				printDLL();
				break;

			case 8:
				exit(0);

			case 9:
				{
				countNode();
				
				break;
			}
			

			default:
				printf("\n WRONG CHOICE");
				break;
		    }
		}
	
}


