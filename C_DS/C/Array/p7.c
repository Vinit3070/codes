// Sum of Array Elements
#include<stdio.h>

void main(){

	int size;
	printf("\nEnter the Size::");
	scanf("%d",&size);

	int arr[5];
	printf("\nEnter array Elements::");
	for(int i=0;i<size;i++)
	{
		scanf("%d",&arr[i]);
	}

	int sum=0;
	for(int i=0;i<size;i++)
	{
		sum=sum+arr[i];
	}
	printf("\nSUM of Array Elements are: %d",sum);
	printf("\n");
}
