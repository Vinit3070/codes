//comparing two arrays with their elements
#include<stdio.h>
void main(){

	int arr1[3]={10,20,30};
	int arr2[3]={50,25,40};
/*	
	if(arr1==arr2)
	{
		printf("Equal");
	}
	else{
		printf("Not Equal");
	}
	// Direct Comparing is not Possible so compare with array elements....

*/
	int flag=0;
	for(int i=0;i<3;i++)
	{
		if(arr1[i]==arr2[i])
		{
			flag=1;
		}
		else
		{
			flag=0;
		}
	}
	if(flag==1)
	{
		printf("Array's are Equal...\n");
	}
	else
	{
		printf("Array's are Not Equal...\n");
	}
	
	printf("\n");
}
