// Assigning  the array to another array...

#include<stdio.h>
void main(){
	
	int arr1[5];
	printf("\nEnter Array Elements:");
	for(int i=0;i<5;i++)
	{
		scanf("%d",&arr1[i]);
	}

	printf("Array Elements of First Array are::\n");
	for(int i=0;i<5;i++)
	{
		printf("%d ",arr1[i]);
	}
	
	int arr2[5];
	
	//arr2=arr1;		//ERROR----> [ Assignment to expression with array...]
	


	//Copy Elements of 1st array to 2nd array...
	for(int i=0;i<5;i++)
	{
	    arr2[i]=arr1[i];
	}

	printf("\nArray Elements of Second Array are::");
	for(int i=0;i<5;i++)
	{
		printf("%d ",arr2[i]);
	}

	printf("\n");
}

