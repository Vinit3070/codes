//Print 2d array diagonally....

#include<stdio.h>
void main(){

	int arr[3][3];
	int sum=0;

	printf("\nENTER ARRAY ELEMENTS::\n");
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			scanf("%d ",&arr[i][j]);
		}
	}
	printf("\nENTERED ELEMENTS ARE::\n");
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			printf("\t%d",arr[i][j]);
		}
		printf("\n");
	}
	
	printf("PRINTING DIAGONAL ARRAY::\n");
	for(int i=0;i<3;i++)
	{	
		
		for(int j=0;j<3;j++)
		{
			if(i==j)
			{
				printf("\t%d\n",arr[i][j]);
				sum=sum+arr[i][j];
			}
			//printf("\n");
		}

	}
	printf("\nSUM = %d\n",sum);
	printf("\n");
}


