//Simple 2d Array - Take User Input and Printing elements....Initiatlization way 2

#include<stdio.h>
void main(){

	int arr[3][3]={{1,2,3,4},{5,6},{7,8,9}};	//Initialisation way 2

	printf("\nENTER ARRAY ELEMNTS::\n");
	/*for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			scanf("%d",&arr[i][j]);
		}
	}*/

	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			printf("%d ",arr[i][j]);
			
		}
		printf("\n");
	}
	printf("\n");

}
/*
 O/p---->
 			
   	1  2  3

	5  6  0		// In o/p 4 is not considered because row only access 3 elements so skip 4 
			//in second row empty position is filled with 0.
	7  8  9
*/

