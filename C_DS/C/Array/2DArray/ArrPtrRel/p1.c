#include<stdio.h>
void main(){

	int arr[3][3]={10,20,30,40,50,60,70,80,90};

	printf("\nARRAY ELEMENTS ARE::\n");
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			printf("\t%d",arr[i][j]);
		}
		printf("\n");
	}

	printf("%d\n",arr[0][0]);		//10
	printf("%d\n",arr[1][1]);		//50
	printf("%d\n",arr[2][2]);		//90


	printf("%p\n",&arr[0][0]);		//0x100			100  104  108  111
	printf("%p\n",&arr[1][1]);		//0x116			112  116  120  123 
	printf("%p\n",&arr[2][2]);		//0x132			124  128  132  135
	
}
/*
 	Internal WORKING-->

  arr[1][1]= *[ *(arr+1)+1 ]
  	   = *[ *( 100+1 * sizeof ( whole 1 DArray ) + 1 * sizeof ( DT of Ptr ) )]
           = *[ 100 + 1 * 12 ] + 1 * 4]	  
	   = *[112+4]
	   = *(116)
	   
	   ValueAt(116)= 50

*/
