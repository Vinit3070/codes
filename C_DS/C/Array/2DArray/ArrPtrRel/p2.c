/*
 	    1     2    	3
	100   104   108	  111
	    4     5     6
	112   116   120   123			QUE--> arr[1][2]
	    7     8     9			        (120) 
	124   128   132   135				[6]

*/
#include<stdio.h>
void main(){

	int arr[3][3]={1,2,3,4,5,6,7,8,9};

	printf("\nENTERED ARRAY ELEMENTS ARE::\n");
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			printf("\t%d",arr[i][j]);
		}
		printf("\n");
	}

	printf("ADD of arr[1][2]--> %p\n",&arr[1][2]);			//0x120
	printf("ACTUAL VAL OF - arr[1][2]--> %d\n",arr[1][2]);		//6

}

/*
		arr[1][2]   =	*[(arr + 1(size of whole 1 d Array) +2*sizeof(DT of Ptr))]
	       			*[ (100 + 1 * (sizeof(whole 1 D ARRAY)) + 2 * sizeof(DT of ptr)) ]
				*[( 100 + 1 * 12 ) + 2 * 4 ]
				*[112+8]
				*(120)

			ValueAt(120)= 6
*/

