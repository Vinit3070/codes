//Accessing array elements with [array of pointer]
#include<stdio.h>
void main(){

	int x= 10;
	char ch= 'A';
	double d= 10.55;

	void *arr[3]={&x,&ch,&d};

	printf("\nADDRESS::\n");
	printf("x=%p\n",arr[0]);				//0x101
	printf("ch=%p\n",arr[1]);				//0x100
	printf("d=%p\n",arr[2]);				//0x105
/*	
 
   DEREFERENCING OF VOID POINTER IS NOT ALLOWED DIRECTLY ,WE HAVE TO TYPECAST IT WITH ORIGINAL DATATYPE...
	printf("\nDATA::\n");
	printf("%d\n",*(arr[0]));			//10
	printf("%c\n",*(arr[1]));			//A
	printf("%f\n",*(arr[2]));			//10.55
							
*/
	printf("\nACTUAL DATA::\n");
	printf("%d\n",*((int *)(arr[0])));		//10		DEREFERENCING into INT*
	printf("%c\n",*((char *)(arr[1])));		//A		DEREFERENCING into CHAR*
	printf("%f\n",*((double *)(arr[2])));		//10.55		DEREFERENCING into DOUBLE*

}
