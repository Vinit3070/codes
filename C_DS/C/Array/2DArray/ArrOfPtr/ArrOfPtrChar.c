//Char (Array of pointer)

#include<stdio.h>
void main(){

	char arr1[3]={'A','B','C'};
	char arr2[3]={'D','E','F'};
	char arr3[3]={'G','H','I'};

	char *cparr[3]={arr1,arr2,arr3};

	printf("\nSIZE OF CHARACTER POINTERS ARRAY IS : %ld",sizeof(cparr));

	cparr[0]=arr1+1;				// A+1-->B
	cparr[1]=arr2+2;				// D+2-->F
	cparr[2]=arr3;					// G 

	printf("\nIncrement Operations");

	printf("\n0-%c",*(cparr[0]));			//B
	printf("\n1-%c",*(cparr[1]));			//F
	printf("\n2-%c\n",*(cparr[2]));			//G

}

