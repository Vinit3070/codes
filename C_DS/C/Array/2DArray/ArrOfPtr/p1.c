/*
 								 0	       1	     2
	x=10		y=20		z=30          *	arr-   [100]   	     [104]         [108]
      100   103	     104   107	     108    111		     200   207     208   215     216   223
 


*/

#include<stdio.h>
void main(){

	int x=10;
	int y=20;
	int z=30;

	int * arr[3]={&x,&y,&z};

	printf("SIZE_OF_ARRAY_POINTER: %ld\n",sizeof(arr));
	
	printf("\nADDRESS\n");
	printf("%p\n",arr[0]);			//100
	printf("%p\n",arr[1]);			//104
	printf("%p\n",arr[2]);			//108
	
	printf("\nDATA::\n");
	printf("%d\n",*(arr[0]));			//10
	printf("%d\n",*(arr[1]));			//20
	printf("%d\n",*(arr[2]));			//30

}
