//SUM OF ARRAY...
#include<stdio.h>
void main(){

	int rows,cols;
	printf("\nENTER ROWS:");
	scanf("%d",&rows);
	printf("\nENTER COLS:");
	scanf("%d",&cols);

	int sum=0;
	int arr[rows][cols];
	printf("\nENTER ARRAY ELEMENTS::");
	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<cols;j++)
		{
			scanf("%d",&arr[i][j]);
			sum=sum + arr[i][j];
		}
	}

	printf("\nARRAY ELEMENTS\n");
	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<cols;j++)
		{
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	printf("\nSUM OF ARRAY = %d\n",sum);
}
