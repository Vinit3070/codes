/* 
  	POINTER TO AN ARRAY
 


	[ARR]  	 	 1   	2      3      4      5
    ptr 1=ptr 2==	100    104    108    115    119
     	^	 	    	     ptr 3
        |
        |
     (*ptr 4)

*/
			      
#include<stdio.h>

void main(){
	
	int arr[5]={1,2,3,4,5};

	int *ptr1= arr;			//Add of First Element
	int *ptr2= &arr;		//Whole ARR's Address
	int *ptr3 =&arr[2];		//Gives us ValAt 3 rd index ---->[3]

	int (*ptr4)[5]= &arr ;		//Pointer to an Array gives us Whole Array's Address.
					

	// AT Arrays Address first element can be print in our O/P.[1]

	printf("%d\n",*ptr1);				//1
	printf("%d\n",*ptr2);				//1
	printf("%d\n",*ptr3);				//3
	printf("%p\n",*ptr4);				//WHOLE ARRAY's ADRRESS

	printf("%p\n",&arr);				//Arrays address
							
}
