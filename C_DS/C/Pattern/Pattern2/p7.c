/*Pattern2-7

	1   2   3   4
	25  36  49  64
	9   10  11  12
	169 194 225 256
*/
#include<stdio.h>
void main(){

	int rows,num=1;
	printf("\n Enter the rows:");
	scanf("%d",&rows);

	printf("\n\t");
	for (int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			if(i%2!=0)
			{
				printf("%d\t",num);
				num++;
			}
			else
			{
				printf("%d\t",num*num);
				num++;
			}
		}
		printf("\n\t");
	}
	printf("\n\n");
}
