/*Pattern2-6
 	
	= = = = 
	$ $ $ $
	= = = =
	$ $ $ $
 
*/
#include<stdio.h>
void main(){

	int rows;
	printf("\n Enter the rows:");
	scanf("%d ",&rows);

	printf("\n\t");

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			if (i%2!=0)
			{
				printf("$\t");
			}
			else
			{
				printf("=\t");
			}
		}
		printf("\n\t");
	}
	printf("\n\n");
}
