/*Pattern2-2- 
	
  	3 2 1
	c b a
	3 2 1

*/
#include <stdio.h> 
void main(){
	
	int rows;
	char ch;
	printf("\n Enter the rows: ");
	scanf("%d",&rows);
	
	printf("\n\t");

	for(int i=1;i<=rows;i++)
	{	
		ch=95+rows;
		for (int j=rows-1;j>=1;j--)
		{
			if(i%2!=0)
			{
				printf("%d  ",j);
			}
			else
			{
				printf("%c  ",ch);
				ch--;
			}
			
		}
		printf("\n\t");
	}
	printf("\n\t");
}

