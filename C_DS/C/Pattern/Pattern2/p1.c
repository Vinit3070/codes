/*Pattern2-1

	4 3 2 1 
	5 4 3 2
        6 5 4 3 
 	7 6 5 4 	
*/
#include<stdio.h>
void main(){

	int rows,num;
	printf("\n Enter the rows: ");
	scanf("%d",&rows);
	printf("\n\t");
	num=rows;

	for (int i=1;i<=rows;i++)
	{
		for (int j=1; j<=rows;j++)
		{
			printf(" %d\t ",num);
			num--;
		}
		num=num+1;
		num=num+rows;
		printf("\n\t");
	}
	printf("\n");
}
