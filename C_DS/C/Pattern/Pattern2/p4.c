/*Pattern2-4
 
	I H G
	F E D
	C B A
*/

#include<stdio.h>
void main(){

	int rows; 
	char ch;
	printf("\n Enter the rows:");
	scanf("%d",&rows);
	printf("\n\t");

	ch=64+rows*rows;

	for(int i=1;i<=rows;i++)
	{
		for (int j=1;j<=rows;j++)
		{
			printf("%c\t",ch);
			ch--;
		}
		printf("\n\t");
	}
	printf("\n\n");
}
