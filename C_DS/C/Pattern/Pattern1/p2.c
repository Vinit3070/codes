/*Pattern1-2
 
 	1 2 3
	a b c
	1 2 3
	a b c
*/

#include<stdio.h>
void main(){

	int rows;

	printf("\n Enter The Number of rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		int x=1;
		char ch='a';

		for (int j=1;j<=rows;j++)
		{
			if(i%2==0)
			{
				printf("%d ",x);
				x++;
			}
			else
			{
				printf("%c ",ch);
				ch++;
			}
		}
	printf("\n");
	}
}


