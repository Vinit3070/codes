/*Pattern1-9
 	2  5  10
	17 26 37
	50 65 82
*/

#include<stdio.h>
void main(){

	int rows,num=1,a=1,result=0;

	printf("\n Enter The Rows: ");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{

			result = a + num;
			printf("\t%d ",result);
			a = result;
			num=num+2;
		}

		printf("\n\n");
	}
}
