/*Pattern1-10
 
	D4 C3 B2 A1
	D4 C3 B2 A1
	D4 C3 B2 A1
	D4 C3 B2 A1
 
*/

#include<stdio.h>
void main(){

	int rows,num;
	printf("\n Enter the Rows:");
	scanf("%d",&rows);

	num=rows;

	for(int i=1;i<=rows;i++)
	{
		char ch=64+rows;

		for(int j=rows;j>=1;j--)
		{
			printf("%c%d\t",ch,j);

			ch--;
		}
		printf("\n");
	}
	printf("\n\n");
}

