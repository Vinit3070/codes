/*Pattern1-7
 *
 	1   2  9   4
	25  6  49  8
	81  10 121 12
	169 14 225 16

*/
#include<stdio.h> 
void main(){
	
	int rows,num=1;
	printf("\n Enter the Rows:");
	scanf("%d",&rows);
	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			if(j%2==0)
			{
				printf("%d ",num);
			}
			else
			{
				printf(" %d   ",num*num);
			}
			num++;
		}
		printf("\n");
	}
}


