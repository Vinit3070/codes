/* PATTERN -
	1  2  3  4
	a  b  c  d
	5  6  7  8
	e  f  g  h
*/
#include<stdio.h>
void main(){

	int rows,num=1;
	char ch='a';
	printf("\n Enter the rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			if(i%2!=0)
			{
				printf("\t%d",num);
				num++;
			}
			else
			{
				printf("\t%c ",ch);
				ch++;
			}
	
		}
		printf("\n");
	}
	printf("\n");
}

