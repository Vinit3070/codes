/*Pattern-
	A  A  A
	B  B  B 
	C  C  C

*/
#include<stdio.h>
void main(){

	int rows;
	char ch='A';
	printf("\n Enter the rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%c",ch);
		}
		ch++;
		printf("\n");
	}
	printf("\n");
}
