/*Pattern-
 
  	a b c 
	d e f 
	g h i
*/
#include<stdio.h>
void main(){

	int rows;
	char ch='a';
	printf("\n Enter the rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%c ",ch);
			ch++;
		}
		printf("\n");
	}
	printf("\n");
}
