/*Pattern-
 
  	1 1 1
	2 2 2 
	3 3 3 
*/
#include<stdio.h>
void main(){

	int rows;
	printf("\n Enter the rows:");
	scanf("%d",&rows);
	int num=1;

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%d",num);
		}
		num++;
		printf("\n");
	}
	printf("\n");
}
