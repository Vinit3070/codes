/*Pattern-
	4 4 4 4 
	3 3 3 3 
	2 2 2 2 
	1 1 1 1
*/
#include<stdio.h>
void main(){

	int rows,num=4;
	printf("\n Enter the rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%d",num);
		}
		num--;
		printf("\n");
	}
	printf("\n");
}

