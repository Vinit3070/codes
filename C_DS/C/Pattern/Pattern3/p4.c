/* Take the rows and cols from the user and print the PATTREN
 
	1  2  3  4
	5  6  7  8 
	9  10 11 12
*/

#include<stdio.h>
void main(){

	int rows,cols,num=1;
	printf("\n Enter the Rows:");
	scanf("%d",&rows);
	printf("\n Enter the Columns:");
	scanf("%d",&cols);
	cols=rows+1;

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=cols;j++)
		{
			printf("\t%d ",num);
			num++;
		}
		printf("\n");
	}
	printf("\n");
}
