/*Pattern8-
	9   64  7
	36  5   16
	3   4   1
*/
#include<stdio.h>
void main(){

	
	int rows;
	printf("\nEnter the number of rows::");
	scanf("%d",&rows);
	int num=9;

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			if(j%2!=0)
			{
				printf("\t%d",num);
				num--;
			}
			else
			{
				printf("\t%d",num*num);
				num--;
			}
			printf("\t");
		
		}
		printf("\n");
		
		
	}
}
