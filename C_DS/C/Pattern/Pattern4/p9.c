/*Pattern-
 	1  3   5 
	5  7   9
	9  11  13
*/

#include<stdio.h>
void main(){

	int num=1;
	int rows;
	printf("\nEnter no of rows:: ");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%d",num);
			num=num+2;
		}
		num=num-2;
		
		printf("\n");
	}
	printf("\n");
}
