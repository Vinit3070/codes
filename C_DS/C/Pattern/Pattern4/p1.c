/*Wap to print the odd numbers as it is and cube of even numbers between a given range from the user*/

#include<stdio.h>
void main(){

	int ip1,ip2;
	printf("\n Enter the Start Range: ");
	scanf("%d",&ip1);
	printf("\n Enter the End Range: ");
	scanf("%d",&ip2);

	for(int i=ip1;i<=ip2;i++)
	{
		if(i%2==0)
		{
			printf("\n%d",i*i*i);
		}
		else
		{
			printf("\n%d",i);
		}
		printf("\n");
	}
	printf("\n");
}
	
