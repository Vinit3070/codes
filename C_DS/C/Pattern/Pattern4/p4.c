/*Pattern-

	a  B  c
	d  E  f
	g  H  i
*/
#include<stdio.h>
void main(){

	int rows;

	printf("\n Enter the rows:");
	scanf("%d",&rows);

	char ch='a';

	for(int i=1;i<=rows;i++)
	{	
		for(int j=1;j<=rows;j++)
		{
			if(j%2!=0)
			{
				printf("\t%c",ch);
			}
			else
			{
				printf("\t%c",ch-32);
			}
			ch++;
		}
		printf("\n");
	}
	printf("\n\n");
}
