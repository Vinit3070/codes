/*Pattern-
 
  	d  d  d  d
	c  c  c  c
	b  b  b  b
	a  a  a  a
*/
#include<stdio.h>
void main(){

	int rows;
	char ch='d';
	printf("\n Enter the rows:");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%c",ch);
			
		}
		ch--;
		printf("\n");
	}
	printf("\n");
}
