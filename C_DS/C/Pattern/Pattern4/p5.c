/*Pattern-
	1  2  3
	2  3  4
	3  4  5
*/
#include<stdio.h>
void main(){

	int rows;
	printf("\n Enter the rows:");
	scanf("%d",&rows);
	int num=1;

	for(int i=1;i<=rows;i++)
	{
		
		for(int j=1;j<=rows;j++)
		{
			printf("\t%d",num);
			num++;
		}
		num=num+1;
		num=num-rows;
		printf("\t\n");
		
	}
	printf("\n");
}
