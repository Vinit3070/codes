/*Pattern6-
	 
	D D D D
	C C C C
	B B B B
	A A A A
*/
#include<stdio.h>
void main(){

	int rows;
	printf("\n Enter No of Rows::");
	scanf("%d",&rows);

	char ch='D';

	for(int i=1;i<=rows;i++)
	{
		for(int j=1;j<=rows;j++)
		{
			printf("\t%c",ch);
		}
		ch--;
		printf("\n");
	}
}

