/*Wap that takes numbers from 1-5 and prints its spellings. If the number is greater than 5,then print entered number is greater than 5*/
#include<stdio.h>
void main(){

	int num;
	printf("\n Enter the Number:");
	scanf("%d",&num);

	switch (num){

		case 1:
			printf("\n One");
			break;
		case 2:
			printf("\n Two");
			break;
		case 3:
			printf("\n Three");
			break;
		case 4:
			printf("\n Four");
			break;
		case 5:
			printf("\n Five");
			break;
		default:
			printf("\n %d is Greater than 5",num);
			break;
		}
	printf("\n\n");
}
