/* Wap in which according to the month number print the number of days in that month*/

#include<stdio.h>
void main(){
	int monthNumber;
	printf("\n Enter the Month number:");
	scanf("%d", &monthNumber);

	switch(monthNumber){

		case 1:
			printf("\n January has 31 days");
			break;
		case 2:
			printf("\n February has 28/29 days");
			break;
		case 3:
			printf("\n March has 31 days");
			break;
		case 4:
			printf("\n April has 30 days");
			break;
		case 5:
			printf("\n May has 31 days");
			break;
		case 6:
			printf("\n June has 30 days");
			break;
		case 7:
			printf("\n July has 31 days");
			break;
		case 8:
			printf("\n August has 30 days");
			break;
		case 9:
			printf("\n September has 31 days");
			break;
		case 10:
			printf("\n October has 30 days");
			break;
		case 11:
			printf("\n November has 31 days");
			break;
		case 12:
			printf("\n December has 30 days");
		
		default:
			printf("\n Wrong input");
			break;
		}
	printf("\n\n");
}
			
