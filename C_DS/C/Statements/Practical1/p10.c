/*Wap , take two characters from user if these characters are equal then print them as it is but if they are unequal then print their difference*/
#include<stdio.h>
void main(){
	int diff;
	char chr1;
	char chr2;

	printf("\n Enter the First Character :");
	scanf(" %c",&chr1);

	printf("\n Enter the Second Character :");
	scanf(" %c",&chr2);

	if((chr1 >= 'A' && chr2 <= 'Z') || (chr1 >=  'a' && chr2 <= 'z'))
	{
		if (chr1 == chr2){
			printf("\n Both Characters Are Equal..");
		}else if( chr1 > chr2){
			printf("\n Both Characters Are Unequal.. ");
			diff= chr1 - chr2;
			printf("\n Difference : %d",diff);
		}else if( chr2 > chr1){
			printf("\n Both Characters Are Unequal..");
			diff= chr2 - chr1;
			printf("\n Difference : %d",diff);
		}
	}

	printf("\n\n");
}
	
