/*Wap to print the Numbers in given range and print their multiplicative inverse */

#include<stdio.h>
void main(){

	float no1,no2;

	printf("\n Enter Input 1 ::");
	scanf("%f",&no1);
	printf("\n Enter Input 2 ::");
	scanf("%f",&no2);

	printf("\n Multiplicative Inverse are:");
	for(float i=no1;i<=no2;i++)
	{
		printf("\n  %0.2f ",1/i);
	}
	printf("\n");
}
