/*Wap to print the Square roots between the given range (100-300)*/

#include<stdio.h> 
void main(){
	
	int i,n1,n2,result=1;

	printf("\n Enter the first no:");
	scanf("%d",&n1);
	printf("\n Enter the second no:");
	scanf("%d",&n2);
	for (int i=n1; i<=n2; i++){

		result=(i/result +result)/2;

		if(result * result ==i)
		{
			printf("\n %d = %d",i,result);
		}
	}
	printf("\n");
}
