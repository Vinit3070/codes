/*Wap to print the Number in Reverse order using While Loop*/

#include<stdio.h> 
void main(){

	int input,rem,sum=0,temp;

	printf("\n Enter The Number: ");
	scanf("%d",&input);
	temp=input;


	while(input!=0)
	{
		rem= input % 10;
		sum=sum *10 + rem ;
		input= input /10;
	}
	printf("\n Reverse of %d is :: %d ",temp,sum);
	printf("\n");
}

