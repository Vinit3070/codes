/*Wap to print all even numbers in reverse order and odd numbers in standard way*/

#include<stdio.h>
void main(){

	for (int i=9; i>=2 ;i--)
	{
		if(i%2==0)
		{
			printf("\n The Even numbers in Reverse order are: %d",i);
		}
	}
	
	printf("\n\n");	

	for(int j=2; j<=9; j++)
	
	{
		if(j%2!=0)
		{
			printf("\n The Odd numbers in Standard way are: %d",j);
		}
	}
	
	printf("\n");
}
