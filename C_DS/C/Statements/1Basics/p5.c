/* Wap to print ASCII values from 1-127 */

#include<stdio.h>
void main(){
	
	printf("ASCII values from 1-127 are::\n");

	for (int i=0; i<=127; i++){
		printf("%c ", i);
	}
}
