/* WAP to print table of 11 in reverse order */

#include<stdio.h>
void main(){

	int no=11;
	printf("The Table of 11 in reverse order is:\n");

	for (int i=10; i>=1; i--){
		printf("%d*%d=%d\n", no,i, (no*i));
	}
}

