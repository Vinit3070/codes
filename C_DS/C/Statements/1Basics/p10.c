/*WAP to print the sum of first 10 odd numbers */

#include<stdio.h>
void main(){
	
	int i,no,sum=0;

	printf("Enter the Maximum value:");
	scanf("%d", &no);
	
	printf("First 10 odd numbers from 1 to %d are:\n",no);
	for (int i=1; i<=no; i++)
	{
		if (i%2!=0)
		{
			printf("%d \n",i);
			sum= sum+i;
		}
	}
	printf("The Sum of First 10 Odd Numbers from 1 to %d= %d \n", no,sum);

	printf("\n\n");
	
}

