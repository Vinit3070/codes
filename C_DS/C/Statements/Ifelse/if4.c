#include<stdio.h>
void main(){
	int x=2;
	printf("Start Main\n");
	if (--x){
		printf("In first if block\n");
	}
	if (++x || x++){
		printf("In second if block\n");
	}
	printf("%d\n",x);
	printf("End main\n");
}
