#include<stdio.h>
void main(){
	 int num;
	 char ch;
	 float rs;
	 double crMoney;

	 printf("\n Enter the Number:");
	 scanf("%d", &num);

	 printf("\n Enter the Float Value:");
	 scanf("%f", &rs);

	 printf("\n Enter the Double Value:");
	 scanf("%lf", &crMoney);

	 printf("\n Enter the Character:\n\n");
	 scanf("%c", &ch);

	 printf("\n num = %d ", num);
	 printf("\n rs = %.2f ", rs);
	 printf("\n crMoney = %.6lf ", crMoney);
	 printf("\n ch =%c \n\n", ch);
 

	 printf("\n Size of Integer is: %ld ", sizeof(num));
	 printf("\n Size of Float is: %ld ", sizeof(rs));
	 printf("\n Size of Double is: %ld ", sizeof(crMoney));
	 printf("\n Size of Character is :%ld", sizeof(ch));
	 
	 printf("\n\n");

}
