//POINTER TO POINTER (Character Pointers)

#include<stdio.h>
void main(){

	char ch='A';

	char *cptr1= &ch;
	char *cptr2= cptr1;
	char **cptr3= &cptr2;

	printf("%c\n",ch);			//A
	printf("%c\n",*cptr1);			//A
	printf("%c\n",*cptr2);			//A
	printf("%c\n",**cptr3);			//A
						
	printf("%p\n",&ch);
	printf("%p\n",&cptr1);
	printf("%p\n",&cptr2);
	printf("%p\n",&cptr3);
}

