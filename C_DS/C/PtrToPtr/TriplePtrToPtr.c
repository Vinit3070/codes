//Pointer to Pointer with ***(Triple Pointer)

#include<stdio.h>
void main(){

	int X=10;

	int *iptr1= &X;
	int **iptr2= &iptr1;
	int **iptr3= &iptr1;
	int ***iptr4= &iptr2;

	printf("%d\n",X);			//10
	printf("%d\n",*iptr1);			//10	
	printf("%d\n",**iptr2);			//10
	printf("%d\n",**iptr3);			//10
	printf("%d\n",***iptr4);		//10

}
/*
  
   X			*iptr1			**iptr2			**iptr3			***iptr4
   10			 0x100			 0x200			 0x200			  0x208
100  103	      200     207	      208     215	      216     223              224     231
 
*/
