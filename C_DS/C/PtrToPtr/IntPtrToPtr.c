/* Pointer to Pointer   (** DOUBLE POINTER)
 
		x		*iptr1{ 0x100 }		*iptr2{ 0x100 }  	*iptr3{ 0x100 }
		20		200	    207		208         215 	216         223
	     100  103

ALL POINTING to X's Add (0x100)...

*/
#include<stdio.h>
void main(){

	int x=20;

	int *iptr1= &x;
	int *iptr2= iptr1;
	int **iptr3= &iptr1;
	int **iptr4= &iptr2;

	printf("%d\n",x);			//20
	printf("%d\n",*iptr1);			//20
	printf("%d\n",*iptr2);			//20
	printf("%d\n",**iptr3);			//20
	printf("%d\n",**iptr4);			//20

}
/*
 	1. *iptr1 stores address of x(0x100)---> valueAt(100) ==>[20]
	2. *iptr2 = iptr1  --> It stores valueAt iptr=(0x100) ==>[20]
	3. **iptr3 = &iptr1 --> It stores Addat iptr1(0x100)  ==>[20]
	4. **iptr4 = &iptr2 --> It stores Add of iptr2(0x208) then VALUEAT(0x208) is Add of x  (0x100) so O/p is ==>[20]


	WORKING OF DOUBLE POINTER (**)

	**iptr3= &iptr2---> suppose IPTR2 add is (0x208) then ,

				1.valueAt(208)==>(0x100)
				2.valueAt(0x100)==> [20]

				so the O/p is 20. 	
*/
