//Asssigning Pointer to an Array 1


#include<stdio.h>
void main(){

	int size;
	printf("\nENTER THE SIZE: ");
	scanf("%d",&size);

	int arr[size];
	printf("\nENTER ARRAY ELEMENTS :");
	for(int i=0;i<size;i++)
	{
		scanf("%d\n",&arr[i]);		//10 20 30 40 50
	}
	printf("\nOLD ARRAY");
	for(int i=0;i<size;i++)
	{
		printf("%d\n ",arr[i]);		//10 20 30 40 50
	}

	int *ptr=&arr[0];			//Pointer Points to the 0 th index i.e, [10]
	printf("POINTER-%d\n",*ptr);  			//10

	*ptr++;					//Now it goes to 1 st index [20]

	*ptr=70;				//Val 20 replaced by 70

	printf("\nNEW ARRAY");
	for(int i=0;i<size;i++)
	{
		printf("%d\n ",arr[i]);		//10 70 30 40 50
	}
}
