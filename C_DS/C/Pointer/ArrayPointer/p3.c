#include<stdio.h>
void main(){

	int arr1[5]={10,20,30,40,50};
	int arr2[5]={60,70,80,90,100};

	int *ptr1=arr1;			//0x100
	int *ptr2=&arr1;		//0x100
	
	int *ptr3= arr1+1;		//0 --->1 st index
	int *ptr4= &arr1+1;		//out of bound

	ptr1++;				//10--->20
	ptr2++;				//10--->20

	
	printf("%d\n",*ptr1);		//20
	printf("%d\n",*ptr2);		//20
	
	printf("%d\n",*ptr3);		//20
	printf("%d\n",*ptr4);		//0 or gv

}


