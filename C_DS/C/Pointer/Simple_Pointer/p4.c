
//Assigning pointers to float,char and integer variables...

#include<stdio.h>
void main(){
		

	int a=50;
	char ch='V';
	float f= 10.5;

	int *ptr1=&a;
	char *ptr2=&ch;
	float *ptr3=&f;
	printf("a=  %d \n",*ptr1);
	printf("ch=  %c \n",*ptr2);
	printf("f=  %f \n",*ptr3);
				 	
	printf("Address a=  %p \n",ptr1);
	printf("Address ch=  %p \n",ptr2);
	printf("Address f=  %p \n",ptr3);
}
