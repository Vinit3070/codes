
//Assignment of global and local variable...

#include<stdio.h>

int a=500;
char ch='V';
float f=20.45;

void main(){

	int b=20;

	int *ptr1=&a;
	char *ptr2=&ch;
	float *ptr3=&f;
	int *ptr4=&b;

	printf("GLOBAL VARIABLE\n");

	printf("%p\n",ptr1);
	printf("%d\n",*ptr1);

	printf("%p\n",ptr2);
	printf("%c\n",*ptr2);
				
	printf("%p\n",ptr3);
	printf("%f\n",*ptr3);

	printf("LOCAL VARIABLE\n");

	printf("%p\n",ptr4);
	printf("%d\n",*ptr4);

}

