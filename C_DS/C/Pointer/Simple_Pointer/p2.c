//Assigning Pointer to integer variable

#include<stdio.h>
void main(){

	int a=50;
	printf("%d\n",a);	//50
	printf("%p\n",&a);	//Address of a

	int *ptr= &a;		//Assigning pointer to variable
	printf("%p\n",ptr);	//Address of a
	printf("%d\n",*ptr);	//50

}
