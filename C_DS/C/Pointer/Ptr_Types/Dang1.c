
/* Dangling pointer..
 	1.Dangling pointer is pointer which is globally declare and it stores the address of data 
	2.Pointer stores the address of x from other function..
 	3.And after function works and pop the pointer ,has same address present in there box..
 	4.There is no effect on address 
	5.And when we use this pointer in another functions it returns the old address and value
	6.It Gives us old value till we assign another pointer to that varible.
 
*/
#include<stdio.h>

int a=10;
int b;
int *ptr=NULL;				//pointer is globally declare..
	      
void fun(){
	      
	int x=30;
	      
	printf("new a=%d\n",a);		//10
	printf("new b=%d\n",b);		//0
	      
	ptr=&x;				//stores the address of x
	      
	printf("old=%d\n",*ptr);		//30
	      
	
}
void main(){

	int y=40;

	printf("old a=%d\n",a);		//10
				
	printf("old b=%d\n",b);		//0

	fun();
					 
	printf("new=%d\n",*ptr);		//30 beacuse dangling pointer store the old address
					
					 
}
	      
