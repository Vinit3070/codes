/* NULL pointer 
 
   1. It is use to avoid the segementation fault due to wild pointer 

   2. We initialise the wild pointer to the null so it is known as Null pointer..
   3. It can declare by two ways =NULL or =0
*/

 #include<stdio.h>
 void main(){

 	int a=50;

        int *iptr=NULL;

 	printf("%d\n",iptr);		//0 / NULL
 	printf("%p\n",iptr);
 
}

