/*
 
  VOID POINTER DEREFERENCING TO INTEGER POINTER 

*/
#include<stdio.h>
void main(){

	int x=10;

	int *iptr=&x;
	void *vptr=&x;
	
	printf("%d\n",x);

	printf("%d\n",*iptr);
	//printf("%d\n",*vptr);			//Dereferencing is not allowed , so we need to typecast it.
				
	printf("%d\n",*((int *)vptr));		//Typecasting of void pointer to actual integer pointer 
}
