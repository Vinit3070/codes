/*
 
  VOID POINTER DEREFERENCING TO CHARACTER POINTER 

*/
#include<stdio.h>
void main(){

	char ch='A';

	char *cptr=&ch;
	void *vptr=&ch;
	
	printf("%c\n",ch);

	printf("%d\n",*cptr);
	//printf("%d\n",*vptr);			//Dereferencing is not allowed , so we need to typecast it.
				
	printf("%d\n",*((char *)vptr));		//Typecasting of void pointer to actual character pointer 
}
