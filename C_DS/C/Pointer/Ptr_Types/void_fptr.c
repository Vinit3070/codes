/*
 
  VOID POINTER DEREFERENCING TO FLOAT POINTER 

*/
#include<stdio.h>
void main(){

	float f1=15.15;

	int *fptr= &f1;
	void *vptr= &f1;
	
	printf("%f\n", f1);

	printf("%f\n",*fptr);
	//printf("%d\n",*vptr);			//Dereferencing is not allowed , so we need to typecast it.
				
	printf("%f\n",*((float *)vptr));		//Typecasting of void pointer to actual float pointer 
}
