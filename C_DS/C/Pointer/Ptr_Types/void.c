
#include<stdio.h>
void main(){

	int x=20;
	int *iptr=&x;

	void *vptr=&x;

	char *cptr=&x;			//Char pointer doesn't access integer variable

	printf("%p\n",iptr);		//Address of x
	printf("%d\n",*iptr);		//20

	//printf("%p\n",vptr);		//
	printf("%d\n",*vptr);		//Dereferencing is Not Allowed..of void pointer
					
	printf("%p\n",cptr);		//Incompatible Pointer type
	printf("%p\n",*cptr);		//It is char * but accessing integer varible so warning is there...
}

/*
 
   VOID POINTER---

   1. Void pointer is generic pointer.
   2. It can store any type of pointers Address(i.e, char,int,double)
   3. Void has size 1 byte.
   4. It cannot Dereference directly.
   5. We have to TYPECAST it for Dereferencing...
   6. We have to specify its actual Datatype.

*/
