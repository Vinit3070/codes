/*
 	WILD POINTER--->
	1. Wild pointer it is very Dangerous pointer.
	2. Use this pointer very carefully.
	 RULES-
	 1.Declarartion is possible
	 2.Initialisation is not Possible.

	 That's why it is dangerous because it can present anywhere so it gives 
	 [ SEGMENTATION FAULT ]

*/
#include<stdio.h>
void main(){

	int x=10;
	int *iptr;			//Declaration allowed but NO Initialisation

	printf("%d\n",*iptr);		//Segmentation Fault/ Garbage value
}
