//Assigning one variables value to another variable...

#include<stdio.h>
void main(){

	int a=50;
	int b=40;

	printf(" a=%d\n b=%d\n",a,b);	//50 40

	a=b;				// Assigning b(40) to [ a ] 

	printf(" a=%d\n b=%d\n",a,b);	//40 40
}


