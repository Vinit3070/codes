
/* Dangling pointer..
 	1.Dangling pointer is pointer which is globally declare and it stores the address of data 
	2.Pointer stores the address of x from other function..
 	3.And after function works and pop the pointer ,has same address present in there box..
 	4.There is no effect on address 
	5.And when we use this pointer in another functions it returns the old address and value
	6.It Gives us old value till we assign another pointer to that varible.
 
*/

//DANGLING POINTER USE WITH CHARACTER....
#include<stdio.h>

char ch='A';
char ch2;

int *cptr=0;				//pointer is globally declare..
	      
void fun(){
	      
	char ch3='C';
	
	      
	printf("new ch=%c\n",ch);		//A
	printf("new ch2=%c\n",ch2);		//blank
	      		
	cptr= &ch3;
	      
	printf("old=%c\n",*cptr);		//C
	      
	
}
void main(){

	char ch2='B';

	printf("old ch=%c\n",ch);		//A
				
	printf("old ch2=%c\n",ch2);		//B

	fun();
					 
	printf("new=%c\n",*cptr);		//C	 beacuse dangling pointer store the old address
					
					 
}
	      
