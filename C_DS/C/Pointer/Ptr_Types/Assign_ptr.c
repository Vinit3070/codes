// Assignment of value of one var to pointer and access it
#include<stdio.h>
void main(){

	int x=50;

	int *ptr=&x;
	printf("x= %d\n",x);			//50
	printf("ptr= %d\n",*ptr);		//50

	*ptr=30;				//Assign 30 to pointer 
						//After Assigning First Pointer value deleted and New is Assigned...
						//so new pointer value becomes 30

	printf("new_ptr= %d\n",*ptr);		//30
	
}

