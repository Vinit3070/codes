/*
 	DANGLING POINTER SCENARIO 2
	1.In that scenario ,if we free then pointer 1 access is gone .
	2.After free it prints the Default type's value.

*/
#include<stdio.h>
#include<stdlib.h>
void DangPointer(int x){

	int *ptr1=(int *)malloc(sizeof(int));

	*ptr1=x;

	printf("%d\n",*ptr1);
	printf("%p\n",ptr1);

	int*ptr2= ptr1;

	printf("%d\n",*ptr2);
	free(ptr1);
	printf("After Free\n");
	printf("%p\n",ptr2);
	printf("%d\n",*ptr2);
}

void main()
{
	DangPointer(30);
}
