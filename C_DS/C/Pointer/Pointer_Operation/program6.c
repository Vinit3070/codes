//Assignemnt of Array to Integer Pointer for Addition and Substraction
#include<stdio.h>
void main(){
	int arr[5]={10,20,30,40,50};

	int *ptr1=arr;
	int *ptr2=&arr;
	int *ptr3=&arr[1];
	int *ptr4=&arr[4];

	printf("%p\n",arr);
	printf("%p\n",&arr);

	printf("%d\n",arr[1]);			//20
	printf("%d\n",arr[4]);			//50

	printf("%d\n",*(ptr3+2));		//40	ADD 2 steps front
	printf("%d\n",*(ptr4-2));		//30	REMOVES 2 steps back

}

	


