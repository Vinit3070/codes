#include<stdio.h>
void main(){

	int x=10;
	int y=20;
	char ch='A';				//65
	char ch2='B';				//66
	float f1=15.5;
	float f2=10.5;

	int *iptr1=&x;
	int *iptr2=&y;
	char *cptr1=&ch;
	char *cptr2=&ch2;
	float *fptr1=&f1;
	float *fptr2=&f2;

	printf("%d\n",*iptr1);
	printf("%d\n",*iptr2);
	printf("%d\n",*cptr1);
	printf("%d\n",*cptr2);
	printf("%f\n",*fptr1);
	printf("%f\n",*fptr2);

	printf("\n");
	printf("%d\n",*(iptr1+1));		//20   ONE STEP FORWARDED SO NEXT VARIBLE
	//printf("%d\n",*(ptr2+1));		//garbage val
	
	printf("\n");

	//printf("%c\n",*(cptr1+65));		//ERROR -- blank /Segementation Fault  ADDITION IS >>>[265]
	printf("%d\n",*(cptr1+1));		//66	Add 1 then it goes to NEXT one...
						
	printf("\n");
	//printf("%f\n",*(fptr1+1.5));		//ERROR -- Invalid Operands
	printf("%f\n",*(fptr1+1));		//10.500000>>> It also Assigns to Next varible after adding 1...


}
//Integer value is Allowed by all types of pointers...


// Char and Char ptr is also not possible ...only char and integer is possible

//FLOAT AND FLOAT POINTER ADDITION IS NOT POSSIBLE ...
//float ptr + int = is possible ...

/*
1.  	*( iptr1 + 1)

  	ValueAt( 100 + 1 * sizeod(DTP))
       
        ValueAt( 100 + 1 * 4)
         
        ValueAt (104) 					 // 20
				 

2.	*( cptr1 + 1)
	ValueAt( 200 + 1 * sizeod(DTP))
	       
	ValueAt( 200 + 1 * 1)
		   
	ValueAt (201)  					// 66[ B ]


3.      *( fptr1 + 1)
   	ValueAt( 300 + 1 * sizeod(DTP))
 
        ValueAt( 300 + 1 * 4)
 
        ValueAt (304)  					// 10.5
		     

*/
