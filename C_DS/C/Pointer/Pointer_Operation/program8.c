#include<stdio.h>
void main(){

	int arr[5]={10,20,30,40,50};

	int *ptr1=&arr[1];
	int *ptr2=&arr[3];

	//printf("%d\n",ptr2+ptr1);	//Addition is not possible
	printf("%d\n",ptr2-ptr1);	//2	3-1	

	printf("%d\n",*ptr2+*ptr1);	//60	40+20
	printf("%d\n",*ptr2-*ptr1);	//20	40-20
}

	// Addition of  Any two pointers is not allowed
	// Addition of one pointer and one integer is allowed...
	
	// Subtraction of any two pointer is allowed...
	// Subtraction of one pointer and one integer is allowed..
	 			
	// Multiplication ,Division, Modulas, is not allowed is pointers.....
	// Generates ERROR--->>>[ Invalid operands ]
