//Pointer Increment and decrement Operations...

#include<stdio.h>


void main(){

	int arr[5]={10,20,30,40,50};
	int *ptr=&arr[0];
	int *ptr2=&arr[4];

	printf("Increment\n");
	printf("%d\n",*(ptr++));		//10
	printf("%d\n",*(++ptr));		//30	it becomes 20 after above increment.so ans is 30...

	printf("Decrement\n");
	printf("%d\n",*(ptr2--));		//50
	printf("%d\n",*(--ptr2));		//30	it becomes 40 after above decrement, so ans is 30...
						
}
