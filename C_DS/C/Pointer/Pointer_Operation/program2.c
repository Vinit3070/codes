
//Addition of Integer ,Character and Float

#include<stdio.h>
void main(){
	int a=10;
	int b=20;

	char ch='A';					//65
	char ch1='B';					//66
	
	float f1=10.5;
	float f2=20.5;
	
	int *iptr1 = &a;
	int *iptr2 = &b;
	char *cptr1 = &ch;
	char *cptr2 = &ch1;
	float *fptr1 = &f1;
	float *fptr2 = &f2;


	printf("a= %d\n",*iptr1);
	printf("b= %d\n",*iptr2);
	printf("ch= %c\n",*cptr1);
	printf("ch1= %c\n",*cptr2);
	printf("f1= %f\n",*fptr1);
	printf("f2= %f\n",*fptr2);

	printf("Integer : %d\n\n",*iptr1 + *iptr2);	//30
	printf("Char : %d\n\n",*cptr1 + *cptr2);	//131
	printf("Float : %f\n\n",*fptr1 + *fptr2);	//31.000000

}

