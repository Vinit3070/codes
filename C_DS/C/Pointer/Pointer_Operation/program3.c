
//Substraction of Integer ,Character and Float

#include<stdio.h>
void main(){
	int a=30;
	int b=20;

	char ch='C';					//67
	char ch1='B';					//66
	
	float f1=20.5;
	float f2=10.5;
	
	int *iptr1 = &a;
	int *iptr2 = &b;
	char *cptr1 = &ch;
	char *cptr2 = &ch1;
	float *fptr1 = &f1;
	float *fptr2 = &f2;


	printf("a= %d\n",*iptr1);
	printf("b= %d\n",*iptr2);
	printf("ch= %c\n",*cptr1);
	printf("ch1= %c\n",*cptr2);
	printf("f1= %f\n",*fptr1);
	printf("f2= %f\n",*fptr2);

	printf("Integer : %d\n\n",*iptr1 - *iptr2);	//10
	printf("Char : %d\n\n",*cptr1 - *cptr2);	//1
	printf("Float : %f\n\n",*fptr1 - *fptr2);	//10.000000

}

