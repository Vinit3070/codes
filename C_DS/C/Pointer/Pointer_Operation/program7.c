

#include<stdio.h>
void main(){

	char carr[]={'A','B','C','D','E'};

	char *cptr1=&carr[1];
	char *cptr2=&carr[3];

	printf("%c\n",*cptr1);			//B
	printf("%c\n",*cptr2);			//D						     
	
	
	printf("%c\n",*(cptr1+2));		//D
	printf("%c\n",*(cptr2-2));		//B

}

/*
 	*(ptr1 + 2 )
 	*(ptr1 + 2 * sizeof(DTP))
	*(101 +2 * 1)
	 ValueAT(103) 

// Garbage value or NULL if it goes out of bound
*/
