#include<stdio.h>
void main(){

	int x=10;
	int y=20;

	int *ptr1=&x;
	int *ptr2=&y;

	printf("%d\n",x);
	printf("%d\n",y);
	printf("%d\n",*ptr1);
	printf("%d\n",*ptr2);

	printf("\n");

	printf("%d\n",*(ptr1+1));		//20
	printf("%d\n",*(ptr2+1));		//garbage val
}

/*
  	*( ptr1 + 1)

  	ValueAt( 100 + 1 * sizeod(DTP))
       
        ValueAt( 100 + 1 * 4)
         
        ValueAt (104) 		 // 20
 */
