

#include<stdio.h>
void main(){

	int arr[]={10,20,30,40,50};
	int *ptr=&(arr[0]);

	printf("%p\n",ptr);			//ox100
			
	printf("%d\n",(*ptr++));		//10 first print and then increment 
						  
	printf("%d\n",(*++ptr));		//30 It takes value from above increment so it becomes previous 20+ 1 postion ahead means ANS->30

	printf("%d\n",(*ptr++));		//30 It takes Previous O/p means 30 after reprint its actual increment value is printed(i.e. 40)
}
