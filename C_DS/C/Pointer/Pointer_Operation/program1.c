
//Addition of Pointer

#include<stdio.h>
void main(){

	int x=10;
	int y=20;

	int *ptr1=&x;
	int *ptr2=&y;

	printf("%d\n",x);
	printf("%p\n",&x);
	
	printf("%d\n",y);
	printf("%p\n",&y);
	
	//printf("ADDITION is : %d",ptr1+ptr2);				//Direct Pointer Addition is Not Possible
	printf("ADDITION of Pointers is :%d \n",*ptr1 + *ptr2);		//After Dereferencing Addition is Possible.... 
}

